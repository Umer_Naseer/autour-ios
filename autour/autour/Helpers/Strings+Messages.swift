//
//  Strings+Messages.swift
//  autour
//
//  Created by huda elhady on 10/07/2021.
//

import Foundation

let msgLoginDescription = "Login to your existing account to access all the features in Autour"
let msgSignup = "Not have an account? Let’s Sign Up"
let msgSignupDescription = "Create your account to access Autour"
let msgLogin = "Already have an account? Let’s Login"
let msgResetPassword = "Set your new password so you can Login and access Autour"

let msgTermsConditions = "By proceeding, you agree to our Terms of Use and confirm you have read our Privacy and Cookie Statement"

