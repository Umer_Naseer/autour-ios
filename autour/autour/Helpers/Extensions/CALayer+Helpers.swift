//
//  CALayer+Helpers.swift
//  autour
//
//  Created by huda elhady on 09/07/2021.
//

import UIKit

extension CALayer {

    func allRound(radius: CGFloat) {
        cornerRadius = radius
        masksToBounds = true
    }

    func round(corners: UIRectCorner, width: CGFloat, height: CGFloat) {

        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        self.mask = maskLayer
    }

    func round(corners: UIRectCorner, width: CGFloat, height: CGFloat, frame: CGRect) -> CAShapeLayer {

        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: frame, byRoundingCorners: corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        self.mask = maskLayer
        return maskLayer
    }

    func bordered(corners: UIRectCorner, width: CGFloat, height: CGFloat, borderColor: UIColor) -> CAShapeLayer {

        self.round(corners: corners, width: width, height: height)

        let borderLayer = CAShapeLayer()
        borderLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        borderLayer.lineWidth = borderWidth
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        addSublayer(borderLayer)
        return borderLayer
    }

    func gradient(colors: [CGColor], locations: [NSNumber], startPoint: CGPoint, endPoint: CGPoint) -> CAGradientLayer {

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = colors
        gradientLayer.locations = locations
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }

    func gradient(colors: [CGColor], locations: [NSNumber], startPoint: CGPoint, endPoint: CGPoint, frame: CGRect) -> CAGradientLayer {

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = colors
        gradientLayer.locations = locations
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }
}

