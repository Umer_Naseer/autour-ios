//
//  String+Extensions.swift
//  autour
//
//  Created by huda elhady on 10/07/2021.
//

import UIKit

extension String {
    func formatAsAttributedString(fontSize: CGFloat, fontColor: UIColor, mainFont: String, boldStringArray: [(string: String, fontSize: CGFloat, fontColor: UIColor, font: String, isUnderline: Bool )]) -> NSMutableAttributedString {
        
        let attrString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: UIFont.init(name: mainFont, size: fontSize)!, NSAttributedString.Key.foregroundColor: fontColor])
        boldStringArray.forEach { str in
            let boldStringRange = (self as NSString).range(of: str.string)
            attrString.setAttributes([NSAttributedString.Key.font: UIFont.init(name: str.font, size: str.fontSize)!, NSAttributedString.Key.foregroundColor: str.fontColor], range: boldStringRange)
            if str.isUnderline {
                attrString.addAttribute(NSAttributedString.Key.underlineStyle,
                                        value: NSUnderlineStyle.single.rawValue,
                                        range: boldStringRange)
            }
        }

        return attrString
    }
}
