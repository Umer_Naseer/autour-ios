//
//  ShadowViewExtension.swift
//  autour
//
//  Created by huda elhady on 12/07/2021.
//

import UIKit

extension UIView {

  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, layerRadius: CGFloat = 10) {
    self.layer.cornerRadius = layerRadius
//    self.layer.borderWidth = 1.0
//    self.layer.borderColor = UIColor.lightGray.cgColor
    self.layer.backgroundColor = self.backgroundColor?.cgColor
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOffset = offSet
    self.layer.shadowRadius = radius
    self.layer.shadowOpacity = opacity
    self.layer.masksToBounds = false
  }

    func addLayerWithShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, layerRadius: CGFloat = 10) {
    
        let shadowLayer = CAShapeLayer()
        
        self.layer.cornerRadius = layerRadius
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds,
                                        cornerRadius: self.layer.cornerRadius).cgPath
        shadowLayer.fillColor = self.backgroundColor?.cgColor//UIColor(named: "login_main_view")?.cgColor
        shadowLayer.shadowColor = color.cgColor
        shadowLayer.shadowOffset = offSet
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = radius
    self.layer.insertSublayer(shadowLayer, at: 0)
    
  }
}
