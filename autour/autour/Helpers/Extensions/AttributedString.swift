//
//  AttributedString.swift
//  autour
//
//  Created by huda elhady on 10/07/2021.
//

import UIKit

extension NSMutableAttributedString {

    public func setAsLink(textToFind: String) {

        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: textToFind, range: foundRange)
        }
    }
}
