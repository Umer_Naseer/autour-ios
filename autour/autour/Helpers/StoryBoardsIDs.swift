//
//  StoryBoardsIDs.swift
//  autour
//
//  Created by huda elhady on 09/07/2021.
//

import Foundation

enum StoryBoardsIDs: String {
    case main = "Main"
    case onboarding = "Onboarding"
    case authentication = "Authentication"
    case HomeTab = "HomeTab"
    case filter = "Filter"
    case more = "More"
    case home = "Home"
    case restaurants = "Restaurants"
    case Mosques = "Mosques"
    case hotels = "Hotels"
    case activity = "Activity"
    case dua = "Dua"
    case services = "Services"
    case donation = "Donation"
    case profile = "Profile"
    case planning = "Planning"
    case contactUs = "ContactUs"
    case discover = "Discover"
    
    var id: String {
        return self.rawValue
    }
}

enum ViewControllersIDs: String {
    case onboardingVC = "OnboardingViewController"
    case signupOptions = "SignupOptionsViewController"
    case loginVC = "LoginViewController"
    case loginNavigationV = "LoginNavigationView"
    case signupVC = "SignupViewController"
    case forgotPasswordVC = "ForgotPasswordViewController"
    case resetPasswordVC = "ResetPasswordViewController"
    case VerificationCodeVC = "VerificationCodeViewController"
    case HomeTabBarController = "HomeTabBarController"
    case resultVC = "ResultViewController"
    case filterVC = "FilterViewController"
    case moreVC = "MoreViewController"
    case HomeCategoryVC = "HomeCategoryViewController"
    case RestaurantsVC = "RestaurantsViewController"
    case FoodTypeVC = "FoodTypeViewController"
    case LocationsListedVC = "LocationsListedViewController"
    case DiscountVC = "DiscountViewController"
    case RestaurantDetailsVC = "RestaurantDetailsViewController"
    case MosquesVC = "MosquesViewController"
    case MosqueDetailsVC = "MosqueDetailsViewController"
    case MapVC = "MapViewController"
    case HotelsVC = "HotelsViewController"
    case HotelDetailsVC = "HotelDetailsViewController"
    case ActivityHomeVC = "ActivityHomeViewController"
    case DetailsVC = "DetailsViewController"
    case DuaVC = "DuaViewController"
    case DuaTypeVC = "DuaTypeViewController"
    case ServicesVC = "ServicesViewController"
    case DonationVC = "DonationViewController"
    case ContactUsVC = "ContactUsViewController"
    case ThankYouVC = "ThankYouViewController"
    case PlanningPlacesViewC = "PlanningPlacesViewController"
    case PlanningVC = "PlanningViewController"
    case PlaceDetailsVC = "PlaceDetailsViewController"
    case TourPlanVC = "TourPlanViewController"
    case DestinationVC = "DestinationViewController"
    case PersonsVC = "PersonsViewController"
    case HotelChoiceVC = "HotelChoiceViewController"
    case FavoriteActivityVC = "FavoriteActivityViewController"
    case RestaurantsListVC = "RestaurantsListViewController"
    case MosquesListVC = "MosquesListViewController"
    case ActivityListVC = "ActivityListViewController"
    case HotelsListVC = "HotelsListViewController"
    case ServicesListVC = "ServicesListViewController"
    case HotelDealVC = "HotelDealViewController"
    case DiscoverMainVC = "DiscoverMainViewController"
    case LocationOptionsVC = "LocationOptionsViewController"
    case BestLocationsVC = "BestLocationsViewController"
    case RestOfWorldVC = "RestOfWorldViewController"
    case SpecificHomeVC = "SpecificHomeViewController"
    case ActivityFilterVC = "ActivityFilterViewController"
    case FilterListVC = "FilterListViewController"
    case RestaurantsFilterVC = "RestaurantsFilterViewController"
    case RestaurantsFilterListV = "RestaurantsFilterListView"
    case OpeningHoursVC = "OpeningHoursViewController"
    case ServiceFilterVC = "ServiceFilterViewController"
    case ServiceFilterListV = "ServiceFilterListView"
    case DonationsDetailsVC = "DonationsDetailsViewController"
    
    var id: String {
        return self.rawValue
    }
}
