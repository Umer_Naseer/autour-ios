//
//  RoundButton.swift
//  autour
//
//  Created by huda elhady on 07/07/2021.
//

import UIKit

class RoundButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.bounds.size.height / 2.0
        self.layer.masksToBounds = true
    }
}
