//
//  LinkTextView.swift
//  autour
//
//  Created by huda elhady on 10/07/2021.
//

import UIKit

extension UITextViewDelegate {
    func textViewDidChangeSelection(_ textView: UITextView) {
        textView.selectedTextRange = nil
    }
}
