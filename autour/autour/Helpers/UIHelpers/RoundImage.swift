//
//  RoundImage.swift
//  autour
//
//  Created by huda elhady on 18/07/2021.
//

import UIKit

class RoundImage: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.bounds.size.height / 2.0
        self.layer.masksToBounds = true
    }
}
