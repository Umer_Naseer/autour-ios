//
//  CommonMethods.swift
//  autour
//
//  Created by huda elhady on 12/07/2021.
//

import UIKit

struct CommonMethods {
    static func createVC(_ viewController: String, storyBoard: String) -> UIViewController {
        return UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    static func setArRootVC(_ viewController: UIViewController) {
        UIApplication.shared.windows.first?.rootViewController = viewController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
