//
//  StartViewController.swift
//  autour
//
//  Created by huda elhady on 06/07/2021.
//

import UIKit

class StartViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func exploreButtonAction(_ sender: UIButton) {
        let rootVC = UIStoryboard(name: StoryBoardsIDs.onboarding.id, bundle: nil).instantiateViewController(withIdentifier: ViewControllersIDs.onboardingVC.id)
        UIApplication.shared.windows.first?.rootViewController = rootVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }


}

