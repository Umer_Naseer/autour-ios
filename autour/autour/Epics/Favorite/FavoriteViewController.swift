//
//  FavoriteViewController.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class FavoriteViewController: UIViewController {
    @IBOutlet weak var activityContainerView: UIView!
    @IBOutlet var containerViews: [UIView]!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FavoriteCategory", let vc = segue.destination as? FavoriteCategoryViewController {
            vc.didSelectFavoriteHandler = {[weak self] in
                self?.categoryChanged(category: $0)
            }
        }
    }
    
    func categoryChanged(category: String)  {
        if category == "Activities" {
            activityContainerView.isHidden = false
            containerViews.forEach{$0.isHidden =  $0 == activityContainerView ? false : true}
        }
    }
}
