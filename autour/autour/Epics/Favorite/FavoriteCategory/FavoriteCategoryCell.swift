//
//  FavoriteCategoryCell.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class FavoriteCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
//    static let identifier = "DestinationCollectionCell"
    
    func configure(category: String) {
        categoryLabel.text = category
    }
    
}
