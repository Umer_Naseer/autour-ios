//
//  FavoriteCategoryViewController.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class FavoriteCategoryViewController: UIViewController {
    let list = ["Restaurants","Activities","Hotels","Food"]
    var selected = ""
    var didSelectFavoriteHandler: ((String)-> Void)!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//   setupCollectionView()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        selected = "Activities"
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: 90 , height: self.view.frame.height - 8)
        collectionView.collectionViewLayout = layout
    }
}

extension FavoriteCategoryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ideintifier = selected == list[indexPath.row] ? "FavoriteSelectedCategoryCell" : "FavoriteCategoryCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ideintifier, for: indexPath) as! FavoriteCategoryCell
        cell.configure(category: list[indexPath.row])
        return cell
    }
    
}

extension FavoriteCategoryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selected = list[indexPath.row]
        didSelectFavoriteHandler?(selected)
        
        collectionView.reloadData()
        collectionView.scrollToItem(at: indexPath, at: .right, animated: false)
    }
}


