//
//  FavoriteActivityViewController.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class FavoriteActivityViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [ReviewType] = [
        ReviewType(image: #imageLiteral(resourceName: "pizza"), name: "Camping in Arolla", location: "6085 Hasliberg Goldern, Switzerland", time: "", distance: 1.0, rating: 4.5), ReviewType(image:  #imageLiteral(resourceName: "italian"), name: " Skiing in the Alps", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.3), ReviewType(image: #imageLiteral(resourceName: "italian"), name: " Mountain biking in Zermatt", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.3)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension FavoriteActivityViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FavoriteActivityCell.identifier, for: indexPath) as! FavoriteActivityCell
        cell.configure(result: list[indexPath.row])
        return cell
    }    
    
}

extension FavoriteActivityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension FavoriteActivityViewController {
    
    
    func openActivityDetails(_ restaurant: ReviewType) {
        navigationController?.pushViewController(DetailsViewController.create(screenTitle: "Activity Details", title: "Horse Holidays France", list: [#imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details")]), animated: true)
    }
}

