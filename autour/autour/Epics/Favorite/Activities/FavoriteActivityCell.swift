//
//  FavoriteActivityCell.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit
import Cosmos

class FavoriteActivityCell: UITableViewCell {
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var timeDistanceLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    static let identifier = "FavoriteActivityCell"
    
    func configure(result: ReviewType) {
        resultImageView.image = result.image
        timeDistanceLabel.text = "\(result.time) • \(result.distance)KM"
        locationLabel.text = result.location
        nameLabel.text = result.name
        ratingView.rating = result.rating
        ratingView.text = "(\(result.rating))"
    }
    
}
