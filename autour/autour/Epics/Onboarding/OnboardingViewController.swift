//
//  OnboardingViewController.swift
//  autour
//
//  Created by huda elhady on 08/07/2021.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var onboardingImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    private var imagesList = [#imageLiteral(resourceName: "onboarding1"), #imageLiteral(resourceName: "onboarding2"), #imageLiteral(resourceName: "onboarding3")]
    private var titlesList = ["Exlore Destinations","Food at one Tap", "Fun Activities"]
    private var subTitlesList = ["Discover the places for your trip in the world and feel great.","Choose on the perfet place where to eat!", "Have fun with a list of different activities for everybody. Family, friends and couple."]
    private var index = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        pageControl.frame = CGRect(x: 0, y: 0, width: 80, height: 50)
        updateOnboarding(index: index)
    }
    
    func updateOnboarding(index: Int) {
        onboardingImageView.image = imagesList[index]
        titleLabel.text = titlesList[index]
        subTitleLabel.text = subTitlesList[index]
        pageControl.currentPage = index
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        if index + 1 <=  imagesList.count - 1 {
            index += 1
            updateOnboarding(index: index)
        } else {
            let rootVC = UIStoryboard(name: StoryBoardsIDs.authentication.id, bundle: nil).instantiateViewController(withIdentifier: ViewControllersIDs.loginNavigationV.id)
            UIApplication.shared.windows.first?.rootViewController = rootVC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }


}

