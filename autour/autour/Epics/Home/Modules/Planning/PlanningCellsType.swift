//
//  PlanningCellsType.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

extension PlanningViewController {
    enum PlanningCellsType {
        case planTrust
        case popularPlaces
        case planTour
        
        var title: String {
            switch self {
            case .planTrust:
                return "They trusted us !"
            case .popularPlaces:
                return "Popular Places that we visited"
            case .planTour:
                return ""
            }
        }
        
        var height: CGFloat {
            switch self {
            case .planTrust:
                return 200
            case .popularPlaces:
                return 165
            case .planTour:
                return 550
            
            }
        }
        
    }
}
