//
//  HotelChoiceViewController.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class HotelChoiceViewController: UIViewController {
    let list = ["1 Star","2 Star","3 Star","4 Star"]
    var selected = ""
    private var didSelectHotelHandler: (([String])-> Void)!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        initUI()
    }

    
    static func create(didSelectHotelHandler: @escaping ([String])-> Void)-> HotelChoiceViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.HotelChoiceVC.id, storyBoard: StoryBoardsIDs.planning.id) as? HotelChoiceViewController {
            vc.didSelectHotelHandler = didSelectHotelHandler
            return vc
        }
        return HotelChoiceViewController()
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        didSelectHotelHandler?([selected])
        dismiss(animated: true, completion: nil)
    }
}

extension HotelChoiceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: DestinationTableCell.identifier, for: indexPath) as! DestinationTableCell
        let image = selected == list[indexPath.row] ? #imageLiteral(resourceName: "grey_green_circle"): nil
        cell.configure(title: list[indexPath.row], image: image)
            return cell
    }
    
}

extension HotelChoiceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = list[indexPath.row]
        tableView.reloadData()
    }
}
