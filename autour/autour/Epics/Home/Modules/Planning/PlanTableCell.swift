//
//  PlanTableCell.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit

class PlanTableCell: UITableViewCell {
    private var planTourHandler: (()-> Void)?
    static let identefier = "PlanTableCell"
       
    
    func configure(planTourHandler: @escaping ()-> Void) {
        self.planTourHandler = planTourHandler
    }
    
    @IBAction func planTourButtonAction(_ sender: UIButton) {
        planTourHandler?()
    }
}
