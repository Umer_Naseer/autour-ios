//
//  PlanningViewController.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

class PlanningViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [PlanningCellsType] = [.popularPlaces,.planTour,.planTrust]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: FoodTypeTableCell.identifier, bundle: nil), forCellReuseIdentifier: FoodTypeTableCell.identifier)
        tableView.register(UINib(nibName: PlanTrustCell.identifier, bundle: nil), forCellReuseIdentifier: PlanTrustCell.identifier)
    }

}

extension PlanningViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .planTrust:
            let cell = tableView.dequeueReusableCell(withIdentifier: PlanTrustCell.identifier, for: indexPath) as! PlanTrustCell
            let typeList: [ReviewType] = [ReviewType(image: #imageLiteral(resourceName: "no_notifications"), name: "Plan", location: "", time: "", distance: 4.5, rating: 4.5), ReviewType(image: #imageLiteral(resourceName: "dua_7"), name: "Plan", location: "", time: "", distance: 4.5, rating: 4.5)]
            cell.configure(list: typeList) {
                [weak self] in self?.openPlanningList(title: $0.name)
            } viewAllHandler: {
                self.openViewAllPlacesType()
            }

            return cell
        case .popularPlaces:
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTypeTableCell.identifier, for: indexPath) as! FoodTypeTableCell
            let typeList: [ImageTitleType] = [ImageTitleType(image: #imageLiteral(resourceName: "service_3"), title: "Japan"), ImageTitleType(image: #imageLiteral(resourceName: "service_1"), title: "Sweden"), ImageTitleType(image: #imageLiteral(resourceName: "tour_pakistan1"), title: "Pakistan"), ImageTitleType(image: #imageLiteral(resourceName: "Belgium"), title: "India")]
            cell.configure(list: typeList, title: list[indexPath.row].title, viewAllHandler: openViewAllPlacesType, selectTypeHandler: {[weak self] in self?.openPlanningList(title: $0.title)})
            return cell
        case .planTour:
            let cell = tableView.dequeueReusableCell(withIdentifier: PlanTableCell.identefier, for: indexPath) as! PlanTableCell
            cell.configure(planTourHandler: {[weak self] in self?.openPlanTour()})
            return cell
        
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension PlanningViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
    }
}

extension PlanningViewController {
    func openViewAllPlacesType() {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.PlanningPlacesViewC.id, storyBoard: StoryBoardsIDs.planning.id), animated: true)
    }
}

extension PlanningViewController {
    func openPlanningList(title: String) {
        let vc = PlaceDetailsViewController.create(listTitle: title)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func openPlanTour() {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.TourPlanVC.id, storyBoard: StoryBoardsIDs.planning.id), animated: true)
    }
}

