//
//  DestinationTableCell.swift
//  autour
//
//  Created by huda elhady on 30/07/2021.
//

import UIKit

class DestinationTableCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImageView: UIImageView!
    static let identifier = "DestinationTableCell"

    func configure(title: String, image: UIImage?) {
        titleLabel.text = title
        titleImageView.image = image ?? nil
    }
    
}
