//
//  DestinationViewController.swift
//  autour
//
//  Created by huda elhady on 30/07/2021.
//

import UIKit

class DestinationViewController: UIViewController {
    let list = ["Los Angeles","Loslenes","Lost City","Lose King","Los Andre","Los Huk","Losing House"]
    var selectedList = [String]()
    private var didSelectDestinationsHandler: (([String])-> Void)!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarContainter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchBarContainter.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 0.7, offSet: CGSize(width: -1, height: 1), radius: 10)
    }
    
    func initUI() {
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.systemBackground
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.systemBackground.cgColor
        searchBar.layer.cornerRadius = 10
        let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        clearButton.tintColor = .white
    }
    
    static func create(didSelectDestinationsHandler: @escaping ([String])-> Void)-> DestinationViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.DestinationVC.id, storyBoard: StoryBoardsIDs.planning.id) as? DestinationViewController {
            vc.didSelectDestinationsHandler = didSelectDestinationsHandler
            return vc
        }
        return DestinationViewController()
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        didSelectDestinationsHandler?(selectedList)
        dismiss(animated: true, completion: nil)
    }
}

extension DestinationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: DestinationTableCell.identifier, for: indexPath) as! DestinationTableCell
        let image = selectedList.contains( list[indexPath.row]) ? #imageLiteral(resourceName: "grey_green_circle"): nil
        cell.configure(title: list[indexPath.row], image: image)
            return cell
    }
    
}

extension DestinationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedList.append(list[indexPath.row])
        tableView.reloadData()
    }
}

extension DestinationViewController: UISearchBarDelegate {
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchBar.text?.count == 1 {
////            AppAnalytics.searchAreaPicker()
//        }
//        performSearch(searchText: searchBar.text ?? "")
//    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        performSearch(searchText: searchBar.text ?? "")
    }
    
    func performSearch(searchText: String) {
//        viewModel.getAreas(citiesWithAreas, in: currentCity, by: searchText.lowercased())
    }
    
//    @objc func donekeyboardAction() {
//        view.endEditing(true)
//    }
}
