//
//  PlanningPlacesViewController.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

class PlanningPlacesViewController: UIViewController {
    
    @IBOutlet weak var categoryCollection:UICollectionView!
    
    var categories: [Category] = [Category(name: "Pakistan", image: #imageLiteral(resourceName: "plan_pakistan")), Category(name: "China", image: #imageLiteral(resourceName: "plan_china")),Category(name: "Dubai", image: #imageLiteral(resourceName: "plan_dubai")), Category(name: "Singapore", image: #imageLiteral(resourceName: "plan_singaphore")),Category(name: "Baku", image: #imageLiteral(resourceName: "plan_paku")),Category(name: "Jordan", image: #imageLiteral(resourceName: "plan_jordan")),Category(name: "France", image: #imageLiteral(resourceName: "plan_france")),Category(name: "India", image: #imageLiteral(resourceName: "plan_india"))]{
        didSet
        {
            categoryCollection.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = categoryCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width:  (view.frame.width - 30) / 2 , height: 160)
        categoryCollection.collectionViewLayout = layout
        categoryCollection.reloadData()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension PlanningPlacesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlanningPlaceTableCell.identefier, for: indexPath) as! PlanningPlaceTableCell
        cell.configure(category: categories[indexPath.row])
        return cell
    }
    
}

extension PlanningPlacesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = PlaceDetailsViewController.create(listTitle: categories[indexPath.row].name)
        
    navigationController?.pushViewController(vc, animated: true)
        
    }
}
