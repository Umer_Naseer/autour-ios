//
//  PlanningPlaceTableCell.swift
//  autour
//
//  Created by huda elhady on 27/07/2021.
//

import UIKit

class PlanningPlaceTableCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    static let identefier = "PlanningPlaceTableCell"
       
    
    func configure(category: Category) {
        categoryImageView.image = category.image
        nameLabel.text = category.name
    }
}
