//
//  PersonsViewController.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class PersonsViewController: UIViewController {
    let list = ["1 Persons","2 Persons","3 Persons","5+ Persons"]
    var selected = ""
    private var didSelectPersonsHandler: (([String])-> Void)!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        initUI()
    }

    
    static func create(didSelectPersonsHandler: @escaping ([String])-> Void)-> PersonsViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.PersonsVC.id, storyBoard: StoryBoardsIDs.planning.id) as? PersonsViewController {
            vc.didSelectPersonsHandler = didSelectPersonsHandler
            return vc
        }
        return PersonsViewController()
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        didSelectPersonsHandler?([selected])
        dismiss(animated: true, completion: nil)
    }
}

extension PersonsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: DestinationTableCell.identifier, for: indexPath) as! DestinationTableCell
        let image = selected == list[indexPath.row] ? #imageLiteral(resourceName: "grey_green_circle"): nil
        cell.configure(title: list[indexPath.row], image: image)
            return cell
    }
    
}

extension PersonsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = list[indexPath.row]
        tableView.reloadData()
    }
}
