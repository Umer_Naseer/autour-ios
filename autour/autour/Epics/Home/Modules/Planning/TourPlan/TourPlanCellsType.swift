//
//  TourPlanCellsType.swift
//  autour
//
//  Created by huda elhady on 30/07/2021.
//

import UIKit

extension TourPlanViewController {
    enum TourPlanCellsType {
        case depatureCell
        case destinationCell
        case departureDateCell
        case personsCell
        case hotelCell
        case messageCell
        case destinationSelectedCell(list: [String])
        case personSelectedCell(list: [String])
        case hotelSelectedCell(list: [String])
        
        var title: String {
            switch self {
            case .depatureCell:
                return "Select departure"
            case .destinationCell:
                return "Select destination"
            case .departureDateCell:
                return "Departure Date - Return Date"
            case .personsCell:
                return "Persons"
            case .hotelCell:
                return "Hotel Recomendations"
            case .messageCell, .destinationSelectedCell, .personSelectedCell, .hotelSelectedCell:
                return ""
            }
        }
        
        var height: CGFloat {
            switch self {
            case .depatureCell, .destinationCell, .departureDateCell,.personsCell, .hotelCell, .destinationSelectedCell, .personSelectedCell, .hotelSelectedCell:
                return 60
            case .messageCell:
                return 255
            }
        }
        
        var identifier: String {
            switch self {
            case .depatureCell:
                return "DepartureCell"
            case .destinationCell, .departureDateCell,.personsCell, .hotelCell:
                return "DestinationCell"
            case .messageCell:
                return "MessageCell"
            case .destinationSelectedCell, .personSelectedCell, .hotelSelectedCell:
                return "DestinationSelectedCell"
            }
        }
        
        var image: UIImage {
            switch self {
            case .depatureCell:
                return #imageLiteral(resourceName: "pin")
            case .destinationCell, .destinationSelectedCell:
                return #imageLiteral(resourceName: "pin")
            case .departureDateCell:
                return #imageLiteral(resourceName: "calendar")
            case .personsCell, .personSelectedCell:
                return #imageLiteral(resourceName: "plan_person")
            case .hotelCell, .hotelSelectedCell:
                return #imageLiteral(resourceName: "heart")
            case .messageCell:
                return #imageLiteral(resourceName: "pin")
            }
        }
        
    }
}
