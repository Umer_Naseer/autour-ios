//
//  TourPlanViewController.swift
//  autour
//
//  Created by huda elhady on 30/07/2021.
//

import UIKit

class TourPlanViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [TourPlanCellsType] = [.depatureCell, .destinationCell, .departureDateCell, .personsCell, .hotelCell, .messageCell]
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(ThankYouViewController.create(message: "We have Recieved your Details, we will send you the plan by email."), animated: true)
    }

}

extension TourPlanViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .depatureCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: list[indexPath.row].identifier, for: indexPath) as! PlanDepatureCell
            cell.configure(title: list[indexPath.row].title, image: list[indexPath.row].image)
            return cell
        case .messageCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: list[indexPath.row].identifier, for: indexPath)
            return cell
        case .destinationSelectedCell(let selectedList), .personSelectedCell(let selectedList), .hotelSelectedCell(let selectedList):
            let cell = tableView.dequeueReusableCell(withIdentifier: list[indexPath.row].identifier, for: indexPath) as! DestinationSelectedCell
            cell.configure(list: selectedList, closeButtonHandler: {_ in })
            return cell
            
//        case .personSelectedCell(let selectedList):
//            let cell = tableView.dequeueReusableCell(withIdentifier: list[indexPath.row].identifier, for: indexPath) as! DestinationSelectedCell
//            cell.configure(list: selectedList, closeButtonHandler: {_ in })
//            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: list[indexPath.row].identifier, for: indexPath) as! PlanDestinationCell
            cell.configure(title: list[indexPath.row].title, image: list[indexPath.row].image)
            return cell
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension TourPlanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let vc = DestinationViewController.create { [weak self] selectedList in
                self?.list[indexPath.row] = .destinationSelectedCell(list: selectedList)
                self?.tableView.reloadData()
            }
            self.present(vc, animated: true, completion: nil)
        } else if indexPath.row == 3 {
            let vc = PersonsViewController.create { [weak self] selectedList in
                self?.list[indexPath.row] = .personSelectedCell(list: selectedList)
                self?.tableView.reloadData()
            }
            self.present(vc, animated: true, completion: nil)
        } else if indexPath.row == 4 {
            let vc = HotelChoiceViewController.create { [weak self] selectedList in
                self?.list[indexPath.row] = .hotelSelectedCell(list: selectedList)
                self?.tableView.reloadData()
            }
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    
}

