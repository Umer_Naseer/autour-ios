//
//  PlaceDetailsViewController.swift
//  autour
//
//  Created by huda elhady on 28/07/2021.
//

import UIKit

class PlaceDetailsViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    
    private var listTitle = "Pakistan"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
        
    }
    
    static func create(listTitle: String)-> PlaceDetailsViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.PlaceDetailsVC.id, storyBoard: StoryBoardsIDs.planning.id) as? PlaceDetailsViewController {
            vc.listTitle = listTitle
            return vc
        }
        return PlaceDetailsViewController()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func planButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.TourPlanVC.id, storyBoard: StoryBoardsIDs.planning.id), animated: true)
    }

}

