//
//  FilterViewController.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

class FilterViewController: UIViewController {
    
    @IBOutlet weak var distanceSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        distanceSlider.setThumbImage(#imageLiteral(resourceName: "green_black_circle"), for: .normal)
    }
    
    
    @IBAction func searchResultButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.resultVC.id, storyBoard: StoryBoardsIDs.filter.id), animated: true)
    }
    
}
