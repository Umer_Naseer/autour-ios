//
//  ResultViewController.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

struct ReviewType {
    let image: UIImage
    let name: String
    let location: String
    let time: String
    let distance: Double
    let rating: Double
}

class ResultViewController: UIViewController {
//    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var resultList: [ReviewType] = [ReviewType(image: #imageLiteral(resourceName: "play") , name: "Camping in Arolla", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "4 min", distance: 1.5, rating: 4.3),
        ReviewType(image: #imageLiteral(resourceName: "sky") , name: "Skiing in the Alps", location: "6085 Hasliberg Goldern, Switzerland", time: "2 min", distance: 1.0, rating: 4.5),
        ReviewType(image: #imageLiteral(resourceName: "camping") , name: "Camping in Arolla", location: "6085 Hasliberg Goldern, Switzerland", time: "4 min", distance: 1.5, rating: 4.3),
        ReviewType(image: #imageLiteral(resourceName: "skiing") , name: " Skiing in the Alps", location: "6085 Hasliberg Goldern, Switzerland", time: "2 min", distance: 1.0, rating: 4.5)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search Results"
        tableView.register(UINib(nibName: ResultTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ResultTableViewCell.identifier)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension ResultViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewCell.identifier, for: indexPath) as! ResultTableViewCell
        cell.configure(result: resultList[indexPath.row], bgColor: UIColor(named: "login_main_view")!)
        return cell
    }
    
    
}

extension ResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}
