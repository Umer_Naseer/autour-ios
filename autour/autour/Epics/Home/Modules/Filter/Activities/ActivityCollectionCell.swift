//
//  ActivityCollectionCell.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

class ActivityCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    static let identefier = "ActivityCollectionCell"
       
    
    func configure(activity: String) {
        nameLabel.text = activity
    }
}
