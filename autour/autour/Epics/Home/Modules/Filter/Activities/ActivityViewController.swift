//
//  ActivityViewController.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

class ActivityViewController: UIViewController {
    @IBOutlet weak var ActivityCollection:UICollectionView!
    
    var activities = ["Camping","Nature", "Fishing", "Skiing", "Travel", "More"]{
        didSet
        {
            ActivityCollection.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = ActivityCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width:  (self.view.frame.width - 66) / 3 , height: 42)
        ActivityCollection.collectionViewLayout = layout
        ActivityCollection.reloadData()
    }
    

}

extension ActivityViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        activities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ActivityCollectionCell.identefier, for: indexPath) as! ActivityCollectionCell
        cell.configure(activity: activities[indexPath.row])
        return cell
    }
    
}


