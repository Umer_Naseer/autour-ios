//
//  ServicesListViewController.swift
//  autour
//
//  Created by huda elhady on 02/08/2021.
//

import UIKit

class ServicesListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var list: [ReviewType] = [
        ReviewType(image: #imageLiteral(resourceName: "camping"), name: "Alfahad Services", location: "Switzerland, Paris", time: "", distance: 1.0, rating: 4.5), ReviewType(image:  #imageLiteral(resourceName: "play"), name: "Zeeshan Tours", location: "Paris, France", time: "", distance: 1.5, rating: 4.3), ReviewType(image: #imageLiteral(resourceName: "camping"), name: "Saniporo", location: "Greecee", time: "", distance: 1.5, rating: 4.3)]
    
    private var listTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
        tableView.register(UINib(nibName: ResultTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ResultTableViewCell.identifier)
    }
    
        static func create(listTitle: String)-> ServicesListViewController {
            if let vc = CommonMethods.createVC(ViewControllersIDs.ServicesListVC.id, storyBoard: StoryBoardsIDs.services.id) as? ServicesListViewController {
                vc.listTitle = listTitle
                return vc
            }
            return ServicesListViewController()
        }

    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let vc = CommonMethods.createVC(ViewControllersIDs.ServiceFilterVC.id, storyBoard: StoryBoardsIDs.services.id)
        self.present(vc, animated: true, completion: nil)
    }
}

extension ServicesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewCell.identifier, for: indexPath) as! ResultTableViewCell
        cell.configure(result: list[indexPath.row], bgColor: UIColor(named: "login_main_view")!)
        return cell
    }
    
}

extension ServicesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(DetailsViewController.create(screenTitle: "Services Details", title: "Alfahad Services", list: [#imageLiteral(resourceName: "service_details"), #imageLiteral(resourceName: "service_details"), #imageLiteral(resourceName: "service_details")]), animated: true)
    }
}




