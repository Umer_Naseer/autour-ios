//
//  ServicesViewController.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

//class ServicesViewController: UIViewController {
//    @IBOutlet weak var searchView: UIView!
//    @IBOutlet weak var tableView: UITableView!
//
//    var list: [ServicesCellsType] = [.serviceType,.popularServices]
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableViewSetup()
//    }
//
//    func tableViewSetup()  {
//        tableView.register(UINib(nibName: FoodTypeTableCell.identifier, bundle: nil), forCellReuseIdentifier: FoodTypeTableCell.identifier)
//        tableView.register(UINib(nibName: MosquesNearTableCell.identifier, bundle: nil), forCellReuseIdentifier: MosquesNearTableCell.identifier)
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        searchView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
//    }
//}
//
//extension ServicesViewController: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return list.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        switch list[indexPath.row] {
//        case .serviceType:
//            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTypeTableCell.identifier, for: indexPath) as! FoodTypeTableCell
//            let typeList: [ImageTitleType] = [ImageTitleType(image: #imageLiteral(resourceName: "service_1"), title: "Bus Services"), ImageTitleType(image: #imageLiteral(resourceName: "service_2"), title: "Visa Services"), ImageTitleType(image: #imageLiteral(resourceName: "service_3"), title: "Travel Services"), ImageTitleType(image: #imageLiteral(resourceName: "service_3"), title: "Electric Services")]
//            cell.configure(list: typeList, title: list[indexPath.row].title, viewAllHandler: openViewAllServicesType, selectTypeHandler: {[weak self] in self?.openServicesList(title: $0.title)})
//            return cell
//        case .popularServices:
//            let cell = tableView.dequeueReusableCell(withIdentifier: MosquesNearTableCell.identifier, for: indexPath) as! MosquesNearTableCell
//            let typeList: [MosquesNearYouType] = [
//                MosquesNearYouType(image: #imageLiteral(resourceName: "service_3-1"), title: "Alfahad Services", location: "Switzerland, Paris", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ex eros, hendrerit vitae urna eu, congue tempor nulla."), MosquesNearYouType(image: #imageLiteral(resourceName: "service_3-1"), title: "Zeeshan Tours", location: "Paris, France", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ex eros, hendrerit vitae urna eu, congue tempor nulla."),MosquesNearYouType(image: #imageLiteral(resourceName: "service_3-1"), title: "Saniporo", location: "Greecee", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ex eros, hendrerit vitae urna eu, congue tempor nulla.")]
//            cell.configure(title: "Popular Services", list: typeList, selectMosqueHandler: {[weak self] in self?.openServiceDetails($0)}, viewAllHandler: {[weak self] in self?.openServicesList(title: "List of Services")})
//            return cell
//
//        }
//    }
//
//    @IBAction func backButtonAction(_ sender: UIButton) {
//        navigationController?.popViewController(animated: true)
//    }
//
//}
//
//extension ServicesViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return list[indexPath.row].height
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////
//    }
//}
//
//extension ServicesViewController {
//    func openViewAllServicesType()  {
//        let list: [Category] = [Category(name: "Bus Services", image: #imageLiteral(resourceName: "service_1")), Category(name: "Visa", image: #imageLiteral(resourceName: "service_2")), Category(name: "Travel", image: #imageLiteral(resourceName: "service_3")), Category(name: "Electric", image: #imageLiteral(resourceName: "service_3")), Category(name: "Plumbing", image: #imageLiteral(resourceName: "service_3"))]
//        navigationController?.pushViewController(FoodTypeViewController.create(list: list, listTitle: "Types of Services", type: .services), animated: true)
//    }
//}
//
//extension ServicesViewController {
//    func openServiceDetails(_ mosque: MosquesNearYouType) {
//        navigationController?.pushViewController(DetailsViewController.create(screenTitle: "Services Details", title: "Alfahad Services", list: [#imageLiteral(resourceName: "service_details"), #imageLiteral(resourceName: "service_details"), #imageLiteral(resourceName: "service_details")]), animated: true)
//    }
//
//    func openServicesList(title: String) {
//        navigationController?.pushViewController(ServicesListViewController.create(listTitle: title), animated: true)
//    }
//
//}


class ServicesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [ServicesCellsType] = [.activityType, .findServices ,.popularActivity, .country, .ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: FoodTypeTableCell.identifier, bundle: nil), forCellReuseIdentifier: FoodTypeTableCell.identifier)
//        tableView.register(UINib(nibName: CountryTableCell.identifier, bundle: nil), forCellReuseIdentifier: CountryTableCell.identifier)
//        tableView.register(UINib(nibName:PopularTableCell.identifier, bundle: nil), forCellReuseIdentifier: PopularTableCell.identifier)
        tableView.register(UINib(nibName: PopularActivityCell.identifier, bundle: nil), forCellReuseIdentifier: PopularActivityCell.identifier)
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension ServicesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .activityType:
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTypeTableCell.identifier, for: indexPath) as! FoodTypeTableCell
            let typeList: [ImageTitleType] = [ImageTitleType(image: UIImage(named: "activity1")!, title: "Nature"), ImageTitleType(image: UIImage(named: "activity2")!, title: "Historical"), ImageTitleType(image: UIImage(named: "activity1")!, title: "Hiking"), ImageTitleType(image: UIImage(named: "activity1")!, title: "Theme Park")]
            cell.configure(list: typeList, title: "Type of Activities", viewAllHandler: openViewAllActivityType, selectTypeHandler: {[weak self] in self?.viewAllActivityList(title: $0.title)})
            return cell
        case .findServices:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
        case .popularActivity:
            let typeList: [Place] = [
                Place(name: "Quais de Montreux", place: "Nature, Family", rating: 4.6, image: #imageLiteral(resourceName: "tour_pakistan2")), Place(name: "Trip Trap Escape Game", place: "Game, Family, Enigme", rating: 4.9, image: #imageLiteral(resourceName: "camping"))]
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularActivityCell.identifier, for: indexPath) as! PopularActivityCell
            
            cell.configure(title: list[indexPath.row].title, list: typeList, widthPercentage: 1.3, showDistance: false, selectActivityHandler: { [weak self] _ in self?.openActivityDetails()}, viewAllHandler: { [weak self] in self?.viewAllActivityList(title: "List of activities")})
            return cell
            
            cell.configure(title: list[indexPath.row].title, list: typeList, widthPercentage: 1.3, showDistance: false, selectActivityHandler: { [weak self] _ in self?.openActivityDetails()}, viewAllHandler: { [weak self] in self?.viewAllActivityList(title: "List of activities")})
            return cell
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            let countries = [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
            cell.configure(title: list[indexPath.row].title, list: countries, widthPercentage: 2.3, selectActivityHandler: { [weak self] _ in })

            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        }
    }
    
    func openLocationsList(country: ImageTitleType) {
        navigationController?.pushViewController(LocationsListedViewController.create(listTitle: "List of Activities in Switzerland", listType: .activities), animated: true)
    }
    
    func openActivityDetails() {
        navigationController?.pushViewController(DetailsViewController.create(screenTitle: "Activity Details", title: "Horse Holidays France", list: [#imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details")]), animated: true)
    }
}

extension ServicesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
}

extension ServicesViewController {
    func openViewAllActivityType()  {
                let list: [Category] = [Category(name: "Bus Services", image: #imageLiteral(resourceName: "service_1")), Category(name: "Visa", image: #imageLiteral(resourceName: "service_2")), Category(name: "Travel", image: #imageLiteral(resourceName: "service_3")), Category(name: "Electric", image: #imageLiteral(resourceName: "service_3")), Category(name: "Plumbing", image: #imageLiteral(resourceName: "service_3"))]
                navigationController?.pushViewController(FoodTypeViewController.create(list: list, listTitle: "Types of Services", type: .services), animated: true)
    }
}

extension ServicesViewController {
    func viewAllActivityList(title: String)  {
        navigationController?.pushViewController(ServicesListViewController.create(listTitle: title), animated: true)
    }
}
