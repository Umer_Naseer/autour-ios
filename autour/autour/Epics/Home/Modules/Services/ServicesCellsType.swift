//
//  ServicesCellsType.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

extension ServicesViewController {
    enum ServicesCellsType {
//        case serviceType
//        case popularServices
//
//        var title: String {
//            switch self {
//            case .serviceType:
//                return "Type of Services"
//            case .popularServices:
//                return "Popular Services"
//            }
//        }
//
//        var height: CGFloat {
//            switch self {
//            case .serviceType:
//                return 165
//            case .popularServices:
//                return 460
//
//            }
        
        case activityType
        case findServices
        case country
        case popularActivity
        case ideaContact
        
        var title: String {
            switch self {
            case .activityType:
                return "Type of Activities"
            case .findServices:
                return ""
            case .country:
                return "Get the best experience in those countries"
            case .popularActivity:
                return "Services in your city"
            
            case .ideaContact:
                return ""
            }
        }
        
        var height: CGFloat {
            switch self {
            case .activityType:
                return 165
            case .findServices:
                return 250
            case .country:
                return 330
            case .popularActivity:
                return 300
            case .ideaContact:
                return 220
            }
        }
        
    }
}
