//
//  ServiceFilterListView.swift
//  autour
//
//  Created by huda elhady on 13/10/2021.
//

import UIKit

class ServiceFilterListView: UIViewController {
    let list = ["Phone repair","Travel agency","Type of services","Type of services2"]
    var selectedList = [String]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarContainter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchBarContainter.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 0.7, offSet: CGSize(width: -1, height: 1), radius: 10)
    }
    
    func initUI() {
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.systemBackground
        searchBar.placeholder = "Search"
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.systemBackground.cgColor
        searchBar.layer.cornerRadius = 10
        let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        clearButton.tintColor = .white

    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
//        didSelectDestinationsHandler?(selectedList)
        dismiss(animated: true, completion: nil)
    }
    
}

extension ServiceFilterListView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: DestinationTableCell.identifier, for: indexPath) as! DestinationTableCell
        let image = selectedList.contains( list[indexPath.row]) ? #imageLiteral(resourceName: "activity_check"): nil
        cell.configure(title: list[indexPath.row], image: image)
            return cell
    }
    
}

extension ServiceFilterListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedList.append(list[indexPath.row])
        tableView.reloadData()
    }
}

extension ServiceFilterListView: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        performSearch(searchText: searchBar.text ?? "")
    }
    
    func performSearch(searchText: String) {

    }

}


