//
//  ServiceFilterViewController.swift
//  autour
//
//  Created by huda elhady on 13/10/2021.
//

import UIKit

class ServiceFilterViewController: UIViewController {
    var selected = ""
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func viewMoreButtonAction(_ sender: UIButton) {
        let vc = CommonMethods.createVC(ViewControllersIDs.ServiceFilterListV.id, storyBoard: StoryBoardsIDs.services.id)
        self.present(vc, animated: true, completion: nil)
    }
    
}
