//
//  MoreViewController.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

class MoreViewController: UIViewController {
    
    @IBOutlet weak var categoryCollection:UICollectionView!
    
    var categories: [Category] = [Category(name: "Restaurants", image: #imageLiteral(resourceName: "food")), Category(name: "Activities", image: #imageLiteral(resourceName: "hiking")), Category(name: "Holidays", image: #imageLiteral(resourceName: "planner")), Category(name: "Hotels", image: #imageLiteral(resourceName: "hotels")), Category(name: "Mosques", image: #imageLiteral(resourceName: "mosque")),Category(name: "Services", image: #imageLiteral(resourceName: "support_service")),Category(name: "Charity", image: #imageLiteral(resourceName: "assossiation_more")),Category(name: "Contact", image: #imageLiteral(resourceName: "contact_more")),Category(name: "Duaa", image: #imageLiteral(resourceName: "dua_more"))]{
        didSet
        {
            categoryCollection.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = categoryCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 20
        layout.itemSize = CGSize(width:  (self.view.frame.width - 65) / 3 , height: 85)
        categoryCollection.collectionViewLayout = layout
        categoryCollection.reloadData()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension MoreViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.identefier, for: indexPath) as! CategoryCell
        cell.configure(category: categories[indexPath.row])
        return cell
    }
    
}

extension MoreViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if categories[indexPath.row].name == "Restaurants" {
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.RestaurantsVC.id, storyBoard: StoryBoardsIDs.restaurants.id), animated: true)
        } else if categories[indexPath.row].name == "Mosques" {
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MosquesVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
        } else if categories[indexPath.row].name == "Hotels" {
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.HotelsVC.id, storyBoard: StoryBoardsIDs.hotels.id), animated: true)
        } else if categories[indexPath.row].name == "Activities"{
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.ActivityHomeVC.id, storyBoard: StoryBoardsIDs.activity.id), animated: true)
        } else if categories[indexPath.row].name == "Duaa"{
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.DuaVC.id, storyBoard: StoryBoardsIDs.dua.id), animated: true)
        }
        else if categories[indexPath.row].name == "Services"{
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.ServicesVC.id, storyBoard: StoryBoardsIDs.services.id), animated: true)
        }else if categories[indexPath.row].name == "Charity"{
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.DonationVC.id, storyBoard: StoryBoardsIDs.donation.id), animated: true)
        } else if categories[indexPath.row].name == "Holidays"{
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.PlanningVC.id, storyBoard: StoryBoardsIDs.planning.id), animated: true)
        } else if categories[indexPath.row].name == "Contact"{
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.ContactUsVC.id, storyBoard: StoryBoardsIDs.contactUs.id), animated: true)
        }
        
    }
}
