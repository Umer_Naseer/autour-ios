//
//  ActivityFilterViewController.swift
//  autour
//
//  Created by huda elhady on 12/10/2021.
//

import UIKit

class ActivityFilterViewController: UIViewController {
    var selected = ""
    
    @IBAction func okButtonAction(_ sender: UIButton) {
//        didSelectHotelHandler?([selected])
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func viewMoreButtonAction(_ sender: UIButton) {
//        didSelectHotelHandler?([selected])
        let vc = CommonMethods.createVC(ViewControllersIDs.FilterListVC.id, storyBoard: StoryBoardsIDs.activity.id)
        self.present(vc, animated: true, completion: nil)
    }
    
}
