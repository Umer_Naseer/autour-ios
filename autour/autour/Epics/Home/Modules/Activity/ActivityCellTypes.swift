//
//  ActivityCellTypes.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

extension ActivityHomeViewController {
    enum ActivityCellTypes {
        case activityType
        case findServices
        case country
        case popularActivity
        case activityNearYou
        case location
        case ideaContact
        
        var title: String {
            switch self {
            case .activityType:
                return "Type of Activities"
            case .findServices:
                return ""
            case .country:
                return "Get the best experience in those countries"
            case .popularActivity:
                return "Popular Activities"
            case .activityNearYou:
                return "Near you"
            case .location, .ideaContact:
                return ""
            }
        }
        
        var height: CGFloat {
            switch self {
            case .activityType:
                return 165
            case .findServices:
                return 250
            case .country:
                return 330
            case .popularActivity, .activityNearYou:
                return 300
            case .location:
                return 86
            case .ideaContact:
                return 220
            }
            
//            case .heyThere:
//                return 250
//            case .places:
//                return 330
//            case .country:
//                return 330
//            case .hotels:
//                return 330
//            case .ideaContact:
//                return 220
        }
        
    }
}

