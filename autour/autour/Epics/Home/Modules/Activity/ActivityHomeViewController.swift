//
//  ActivityViewController.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

class ActivityHomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [ActivityCellTypes] = [.activityType, .findServices ,.popularActivity, .activityNearYou, .location, .country, .ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: FoodTypeTableCell.identifier, bundle: nil), forCellReuseIdentifier: FoodTypeTableCell.identifier)
//        tableView.register(UINib(nibName: CountryTableCell.identifier, bundle: nil), forCellReuseIdentifier: CountryTableCell.identifier)
//        tableView.register(UINib(nibName:PopularTableCell.identifier, bundle: nil), forCellReuseIdentifier: PopularTableCell.identifier)
        tableView.register(UINib(nibName: PopularActivityCell.identifier, bundle: nil), forCellReuseIdentifier: PopularActivityCell.identifier)
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension ActivityHomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .activityType:
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTypeTableCell.identifier, for: indexPath) as! FoodTypeTableCell
            let typeList: [ImageTitleType] = [ImageTitleType(image: UIImage(named: "activity1")!, title: "Nature"), ImageTitleType(image: UIImage(named: "activity2")!, title: "Historical"), ImageTitleType(image: UIImage(named: "activity1")!, title: "Hiking"), ImageTitleType(image: UIImage(named: "activity1")!, title: "Theme Park")]
            cell.configure(list: typeList, title: "Type of Activities", viewAllHandler: openViewAllActivityType, selectTypeHandler: {[weak self] in self?.viewAllActivityList(title: $0.title)})
            return cell
        case .findServices:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
        case .popularActivity:
            let typeList: [Place] = [
                Place(name: "Quais de Montreux", place: "Nature, Family", rating: 4.6, image: #imageLiteral(resourceName: "tour_pakistan2")), Place(name: "Trip Trap Escape Game", place: "Game, Family, Enigme", rating: 4.9, image: #imageLiteral(resourceName: "camping"))]
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularActivityCell.identifier, for: indexPath) as! PopularActivityCell
            
            cell.configure(title: list[indexPath.row].title, list: typeList, widthPercentage: 1.3, showDistance: true, selectActivityHandler: { [weak self] _ in self?.openActivityDetails()}, viewAllHandler: { [weak self] in self?.viewAllActivityList(title: "List of activities")})
            return cell
        case .activityNearYou:
            let typeList: [Place] = [
                Place(name: "Valley of Lauterbrunnen", place: "Nature, Hiking", rating: 4.6, image: #imageLiteral(resourceName: "tour_pakistan2")), Place(name: "Le Jet d’eau", place: "Landscape", rating: 4.9, image: #imageLiteral(resourceName: "camping"))]
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularActivityCell.identifier, for: indexPath) as! PopularActivityCell
            
            cell.configure(title: list[indexPath.row].title, list: typeList, widthPercentage: 1.3, showDistance: true, selectActivityHandler: { [weak self] _ in self?.openActivityDetails()}, viewAllHandler: { [weak self] in self?.viewAllActivityList(title: "List of activities")})
            return cell
        case .location:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityNearTableCell", for: indexPath)
            return cell
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            let countries = [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
            cell.configure(title: list[indexPath.row].title, list: countries, widthPercentage: 2.3, selectActivityHandler: { [weak self] _ in })

            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        }
    }
    
    func openLocationsList(country: ImageTitleType) {
        navigationController?.pushViewController(LocationsListedViewController.create(listTitle: "List of Activities in Switzerland", listType: .activities), animated: true)
    }
    
    func openActivityDetails() {
        navigationController?.pushViewController(DetailsViewController.create(screenTitle: "Activity Details", title: "Horse Holidays France", list: [#imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details")]), animated: true)
    }
}

extension ActivityHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
}

extension ActivityHomeViewController {
    func openViewAllActivityType()  {
        let list: [Category] = [Category(name: "Sking", image: #imageLiteral(resourceName: "activity_1")), Category(name: "Surfing", image: #imageLiteral(resourceName: "activity_2")), Category(name: "Climbing", image: #imageLiteral(resourceName: "activity_1")), Category(name: "Sky Diving", image: #imageLiteral(resourceName: "activity_2")), Category(name: "Riding", image: #imageLiteral(resourceName: "activity_1"))]
        navigationController?.pushViewController(FoodTypeViewController.create(list: list, listTitle: "Types of Activity", type: .activities), animated: true)
    }
}

extension ActivityHomeViewController {
    func viewAllActivityList(title: String)  {
        let vc = ActivityListViewController.create(listTitle: title)
        navigationController?.pushViewController(vc, animated: true)
    }
}


