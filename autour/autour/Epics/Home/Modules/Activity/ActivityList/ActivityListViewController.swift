//
//  ActivityListViewController.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit

class ActivityListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var list: [ReviewType] = [
        ReviewType(image: #imageLiteral(resourceName: "camping"), name: "Camping in Arolla", location: "Hasliberg Goldern", time: "", distance: 1.0, rating: 4.5), ReviewType(image:  #imageLiteral(resourceName: "play"), name: " Skiing in the Alps", location: "Route de Tsallion", time: "", distance: 1.5, rating: 4.3), ReviewType(image: #imageLiteral(resourceName: "camping"), name: " Mountain biking in Zermatt", location: "Route de Tsallion", time: "", distance: 1.5, rating: 4.3)]
    private var listTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
        tableView.register(UINib(nibName: ResultTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ResultTableViewCell.identifier)
    }
    
    static func create(listTitle: String)-> ActivityListViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.ActivityListVC.id, storyBoard: StoryBoardsIDs.activity.id) as? ActivityListViewController {
            vc.listTitle = listTitle
            return vc
        }
        return ActivityListViewController()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let vc = CommonMethods.createVC(ViewControllersIDs.ActivityFilterVC.id, storyBoard: StoryBoardsIDs.activity.id)
        self.present(vc, animated: true, completion: nil)
    }
}

extension ActivityListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewCell.identifier, for: indexPath) as! ResultTableViewCell
        cell.configure(result: list[indexPath.row], bgColor: UIColor(named: "login_main_view")!)
        return cell
    }
    
}

extension ActivityListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailsViewController.create(screenTitle: "Activity details", title: "Horse Holidays France", list: [#imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details")])
        navigationController?.pushViewController(vc, animated: true)
    }
}



