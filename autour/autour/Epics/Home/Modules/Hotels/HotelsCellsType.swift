//
//  HotelsCellsType.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

extension HotelsViewController {
    enum HotelsCellsType {
        case findHotels
        case popularHotels
        case country
        case ideaContact
        
        var title: String {
            switch self {
            case .findHotels, .ideaContact:
                return ""
            case .popularHotels:
                return "Popular Hotels"
            case .country:
                return "Countries"
            }
        }
        
        var height: CGFloat {
            switch self {
            case .findHotels:
                return 250
            case .country:
                return 160
            case .popularHotels:
                return 640
            case .ideaContact:
                return 220
            }
        }
        
    }
}
