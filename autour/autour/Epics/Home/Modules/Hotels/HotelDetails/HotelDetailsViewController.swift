//
//  HotelDetailsViewController.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import Foundation
import EAIntroView

class HotelDetailsViewController: UIViewController {
    @IBOutlet weak var introMainView: EAIntroView!
    
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIntroView()
    }
    
    func setupIntroView() {
        
        self.view.layoutIfNeeded()
        let page1 = EAIntroPage.init(customViewFromNibNamed: "IntroPage")
        (page1?.customView.viewWithTag(12) as! UIImageView).image = #imageLiteral(resourceName: "hotel_details")

        let page2 = EAIntroPage.init(customViewFromNibNamed: "IntroPage")
        
        (page2?.customView.viewWithTag(12) as! UIImageView).image = #imageLiteral(resourceName: "hotel_details")

        let page3 = EAIntroPage.init(customViewFromNibNamed: "IntroPage")
        (page3?.customView.viewWithTag(12) as! UIImageView).image = #imageLiteral(resourceName: "hotel_details")

        self.introMainView.pages = [ page1! , page2! , page3! ]
        
        self.introMainView.skipButton.isHidden = true
        self.introMainView.limitPageIndex = self.introMainView.pages.count - 1
        self.introMainView.scrollingEnabled = true
        self.introMainView.pageControlY = 27
        self.introMainView.pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.1019607843, green: 0.9019607843, blue: 0.6549019608, alpha: 1)
        timer =  Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { (timer) in
            // Do what you need to do repeatedly
            let scrolledIndex = self.introMainView.currentPageIndex + 1
            if scrolledIndex < self.introMainView.pages.count {
                self.introMainView.scrollToPage(for: scrolledIndex, animated: true)
            }
            else{
                self.introMainView.scrollToPage(for: 0, animated: true)
            }
            
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getDealButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.HotelDealVC.id, storyBoard: StoryBoardsIDs.hotels.id), animated: true)
    }
    
}
