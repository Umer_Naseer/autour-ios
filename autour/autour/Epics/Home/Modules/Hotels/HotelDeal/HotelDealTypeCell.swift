//
//  HotelDealTypeCell.swift
//  autour
//
//  Created by huda elhady on 08/08/2021.
//

import UIKit

extension HotelDealViewController {
    enum HotelDealTypeCell {
//        case depatureCell
//        case destinationCell
        case departureDateCell
        case personsCell
        case hotelCell
        case messageCell
//        case destinationSelectedCell(list: [String])
        case personSelectedCell(list: [String])
        case hotelSelectedCell(list: [String])
        
        var title: String {
            switch self {
//            case .depatureCell:
//                return "Select departure"
//            case .destinationCell:
//                return "Select destination"
            case .departureDateCell:
                return "Departure Date - Return Date"
            case .personsCell:
                return "Persons"
            case .hotelCell:
                return "Type of room"
            case .messageCell, .personSelectedCell, .hotelSelectedCell:
                return ""
            }
        }
        
        var height: CGFloat {
            switch self {
            case .departureDateCell,.personsCell, .hotelCell, .personSelectedCell, .hotelSelectedCell:
                return 60
            case .messageCell:
                return 255
            }
        }
        
        var identifier: String {
            switch self {
            case .departureDateCell,.personsCell, .hotelCell:
                return "DestinationCell"
            case .messageCell:
                return "MessageCell"
            case .personSelectedCell, .hotelSelectedCell:
                return "DestinationSelectedCell"
            }
        }
        
        var image: UIImage {
            switch self {
            case .departureDateCell:
                return #imageLiteral(resourceName: "calendar")
            case .personsCell, .personSelectedCell:
                return #imageLiteral(resourceName: "plan_person")
            case .hotelCell, .hotelSelectedCell:
                return #imageLiteral(resourceName: "heart")
            case .messageCell:
                return #imageLiteral(resourceName: "pin")
            }
        }
        
    }
}

