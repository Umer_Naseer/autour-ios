//
//  HotelsListViewController.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit

//class HotelsListViewController: UIViewController {
//    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var titleLabel: UILabel!
//
//    var list: [ReviewType] =
//        [ReviewType(image: #imageLiteral(resourceName: "play") , name: "Pullman Paris Tour Eiffel", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "4 min", distance: 1.5, rating: 4.3),
//            ReviewType(image: #imageLiteral(resourceName: "sky") , name: "Hotel Gletscherblick", location: "6085 Hasliberg Goldern, Switzerland", time: "2 min", distance: 1.0, rating: 4.5)]
//    private var listTitle: String = ""
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        titleLabel.text = listTitle
//    }
//
//    static func create(listTitle: String)-> HotelsListViewController {
//        if let vc = CommonMethods.createVC(ViewControllersIDs.HotelsListVC.id, storyBoard: StoryBoardsIDs.hotels.id) as? HotelsListViewController {
//            vc.listTitle = listTitle
//            return vc
//        }
//        return HotelsListViewController()
//    }
//
//    @IBAction func backButtonAction(_ sender: UIButton) {
//        navigationController?.popViewController(animated: true)
//    }
//}
//
//extension HotelsListViewController: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return list.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTableCell.identifier, for: indexPath) as! RestaurantTableCell
//        cell.configure(result: list[indexPath.row])
//        return cell
//    }
//
//}
//
//extension HotelsListViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 120
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.HotelDetailsVC.id, storyBoard: StoryBoardsIDs.hotels.id), animated: true)
//    }
//}

class HotelsListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var list: [Place] = [
        Place(name: "Athens", place: "Grand Hyatt", rating: 4.3, image: #imageLiteral(resourceName: "play") ),
        Place(name: "Hotel Gletscherblick", place: "Switzerland", rating: 4.5, image: #imageLiteral(resourceName: "sky")), Place(name: "Mykonos", place: "Switzerland", rating: 4.5, image: #imageLiteral(resourceName: "sky"))]
    private var listTitle: String = ""
    
    
    static func create(listTitle: String)-> HotelsListViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.HotelsListVC.id, storyBoard: StoryBoardsIDs.hotels.id) as? HotelsListViewController {
            vc.listTitle = listTitle
            return vc
        }
        return HotelsListViewController()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let vc = CommonMethods.createVC(ViewControllersIDs.ActivityFilterVC.id, storyBoard: StoryBoardsIDs.activity.id)
        self.present(vc, animated: true, completion: nil)
    }
}

extension HotelsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HotelListCell.identefier, for: indexPath) as! HotelListCell
        cell.configure(place: list[indexPath.row], showDistance: false)
        return cell
    }
    
}

extension HotelsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.HotelDetailsVC.id, storyBoard: StoryBoardsIDs.hotels.id), animated: true)
    }
}


class HotelListCell: UITableViewCell {
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    static let identefier = "HotelListCell"
       
    
    func configure(place: Place, showDistance: Bool = true) {
//        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        placeImageView.image = place.image
        nameLabel.text = place.name
        placeLabel.text = place.place

            ratingLabel.text = "\(place.rating)"
            ratingLabel.isHidden = false
        distanceLabel.isHidden = !showDistance
        
    }
}
