//
//  HotelsViewController.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

class HotelsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [HotelsCellsType] = [.findHotels,.popularHotels,.ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: CountryTableCell.identifier, bundle: nil), forCellReuseIdentifier: CountryTableCell.identifier)
        tableView.register(UINib(nibName: PopularTableCell.identifier, bundle: nil), forCellReuseIdentifier: PopularTableCell.identifier)
    }

}

extension HotelsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .findHotels:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableCell.identifier, for: indexPath) as! CountryTableCell
            let foodTypeList: [ImageTitleType] = [ImageTitleType(image: #imageLiteral(resourceName: "France"), title: "France"), ImageTitleType(image: #imageLiteral(resourceName: "Switzerland"), title: "Switzerland"), ImageTitleType(image: #imageLiteral(resourceName: "Belgium"), title: "Belgium")]
            cell.configure(list: foodTypeList, selectCountryHandler: {[weak self] in self?.openLocationsList(country: $0)}, backgroundColor: UIColor(named: "home_table")!)
            return cell
        case .popularHotels:
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularTableCell.identifier, for: indexPath) as! PopularTableCell
            let typeList: [ReviewType] =
                [ReviewType(image: #imageLiteral(resourceName: "tour_pakistan3") , name: "Iceland", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "4 min", distance: 1.5, rating: 4.3),
                    ReviewType(image: #imageLiteral(resourceName: "plan_china") , name: "Greece", location: "6085 Hasliberg Goldern, Switzerland", time: "2 min", distance: 1.0, rating: 4.5),
                    ReviewType(image: #imageLiteral(resourceName: "sky") , name: "Japan", location: "6085 Hasliberg Goldern, Switzerland", time: "2 min", distance: 1.0, rating: 4.5)]
            cell.configure(bgColor: UIColor(named: "white_black")!, list: typeList, selectHotelHandler: {[weak self] in self?.openHotelDetails($0)}, viewAllHandler: {[weak self] in  self?.openHotelsList()})
            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func openLocationsList(country: ImageTitleType) {
        navigationController?.pushViewController(LocationsListedViewController.create(listTitle: "Lodging in Switzerland", listType: .hotels), animated: true)
    }
    
    func openHotelDetails(_ hotel: ReviewType) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.HotelDetailsVC.id, storyBoard: StoryBoardsIDs.hotels.id), animated: true)
    }
    
    func openHotelsList() {
        navigationController?.pushViewController(HotelsListViewController.create(listTitle: "Hotels"), animated: true)
    }
    
}

extension HotelsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if list[indexPath.row] == .hotelsNearYou {
//            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MapVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
//        }
    }
}


