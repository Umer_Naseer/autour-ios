//
//  RestaurantsViewController.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

class RestaurantsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [RestaurantsCellsType] = [.foodType, .findRestaurants, .allRestaurant, .theWeekOffers, .bestRestaurants, .halalRestaurants, .country,.ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: FoodTypeTableCell.identifier, bundle: nil), forCellReuseIdentifier: FoodTypeTableCell.identifier)
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
        tableView.register(UINib(nibName: PopularActivityCell.identifier, bundle: nil), forCellReuseIdentifier: PopularActivityCell.identifier)
//        tableView.register(UINib(nibName: PopularRestaurantTableCell.identifier, bundle: nil), forCellReuseIdentifier: PopularRestaurantTableCell.identifier)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension RestaurantsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = list[indexPath.row]
        switch type {
        case .foodType:
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTypeTableCell.identifier, for: indexPath) as! FoodTypeTableCell
            let foodTypeList: [ImageTitleType] = [ImageTitleType(image: #imageLiteral(resourceName: "pizza"), title: "Fast Food"), ImageTitleType(image: #imageLiteral(resourceName: "italian"), title: "Italian"), ImageTitleType(image: #imageLiteral(resourceName: "thai"), title: "Thai"), ImageTitleType(image: #imageLiteral(resourceName: "pakistani"), title: "Pakistani")]
            cell.configure(list: foodTypeList, viewAllHandler: openViewAllFoodType, selectTypeHandler: {[weak self] in self?.openRestaurantsList(title: $0.title)})
            return cell
            
        case .findRestaurants:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FindRestaurantsCell", for: indexPath)
            return cell
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            let countries = [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
            cell.configure(title: list[indexPath.row].title, list: countries, widthPercentage: 2.3, selectActivityHandler: { [weak self] _ in self?.openLocationsList()})

            return cell
            
            
//            let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableCell.identifier, for: indexPath) as! CountryTableCell
//            let countriesList: [ImageTitleType] = [ImageTitleType(image: #imageLiteral(resourceName: "France"), title: "France"), ImageTitleType(image: #imageLiteral(resourceName: "Switzerland"), title: "Switzerland"), ImageTitleType(image: #imageLiteral(resourceName: "Belgium"), title: "Belgium")]
//            cell.configure(list: countriesList, selectCountryHandler: {[weak self] in self?.openLocationsList(country: $0)}, backgroundColor: UIColor(named: "home_table")!)
//            return cell
        case .allRestaurant, .theWeekOffers ,.bestRestaurants:
            let typeList: [Place] = [
                Place(name: "Pizza Lover Company", place: "Hasliberg Goldern, Switzerland", rating: 4.6, image: #imageLiteral(resourceName: "pizza")), Place(name: "Chicken Fried Restaurant", place: "Route de Tsallion", rating: 4.9, image: #imageLiteral(resourceName: "italian"))]
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularActivityCell.identifier, for: indexPath) as! PopularActivityCell
            
            cell.configure(title: list[indexPath.row].title, list: typeList, widthPercentage: 1.3, showDistance: true, selectActivityHandler: { [weak self] _ in self?.openRestaurantDetails()}, viewAllHandler: { [weak self] in self?.openRestaurantsList(title: type.title)})
            return cell
            
            
            
//            let cell = tableView.dequeueReusableCell(withIdentifier: PromotedRestaurantTableCell.identifier, for: indexPath) as! PromotedRestaurantTableCell
//            let typeList: [ReviewType] = [
//                ReviewType(image: #imageLiteral(resourceName: "pizza"), name: "Pizza Lover Company", location: "6085 Hasliberg Goldern, Switzerland", time: "", distance: 1.0, rating: 4.5), ReviewType(image: #imageLiteral(resourceName: "italian"), name: "Chicken Fried Restaurant", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.3)]
//            cell.configure(list: typeList, selectRestaurantHandler: {[weak self] in self?.openRestaurantDetails($0)}, viewAllHandler: {[weak self] in self?.openRestaurantsList(title: "Featured Restaurants")})
//            return cell
//        case .popularRestaurant:
//            let cell = tableView.dequeueReusableCell(withIdentifier: PopularRestaurantTableCell.identifier, for: indexPath) as! PopularRestaurantTableCell
//            let typeList: [ReviewType] = [
//                ReviewType(image: #imageLiteral(resourceName: "pizza"), name: "Pizza Lover Company", location: "6085 Hasliberg Goldern, Switzerland", time: "", distance: 1.0, rating: 4.5), ReviewType(image: #imageLiteral(resourceName: "italian"), name: "Chicken Fried Restaurant", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.3), ReviewType(image: #imageLiteral(resourceName: "thai"), name: "Pizza Lover Company", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.9)]
//            cell.configure(list: typeList, selectRestaurantHandler: {[weak self] in self?.openRestaurantDetails($0)}, viewAllHandler: {[weak self] in self?.openRestaurantsList(title: "Popular Restaurants")})
//            return cell
        case .halalRestaurants:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HalaNearTableCell", for: indexPath)
            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        }
    }
    
    func openLocationsList() {
        navigationController?.pushViewController(LocationsListedViewController.create(listTitle: "Restaurants in Switzerland", listType: .restaurants), animated: true)
    }
    
    func openRestaurantDetails() {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.RestaurantDetailsVC.id, storyBoard: StoryBoardsIDs.restaurants.id), animated: true)
    }
}

extension RestaurantsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
}
// food type
extension RestaurantsViewController {
    func openViewAllFoodType()  {
        let list: [Category] = [Category(name: "Italian Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Pakistani Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Thai Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Fast Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Indian Food", image: #imageLiteral(resourceName: "Italian_food"))]
        navigationController?.pushViewController(FoodTypeViewController.create(list: list, listTitle: "Type of Food", type: .restaurants), animated: true)
    }
}

// food type
extension RestaurantsViewController {
    func openRestaurantsList(title: String)  {
        let vc = RestaurantsListViewController.create(listTitle: title )
        navigationController?.pushViewController(vc, animated: true)
    }
}


