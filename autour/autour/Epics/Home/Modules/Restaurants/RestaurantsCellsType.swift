//
//  RestaurantsCellsType.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

extension RestaurantsViewController {
    enum RestaurantsCellsType {
        case foodType
        case findRestaurants
        case allRestaurant
        case theWeekOffers
        case bestRestaurants
        case halalRestaurants
        case country
        case ideaContact
        
        
        
        var title: String {
            switch self {
            case .foodType:
                return "Type of Food"
            case .findRestaurants, .ideaContact, .halalRestaurants:
                return ""
            case .allRestaurant:
                return "All Restaurants"
            case .theWeekOffers:
                return "This Week’s Offers"
            case .bestRestaurants:
                return "Best Restaurants"
            case .country:
                return "Get the best experience in those countries"
            }
        }
        
        var height: CGFloat {
            switch self {
            case .foodType:
                return 165
            case .country:
                return 330
            case .findRestaurants:
                return 250
            case .ideaContact:
                return 220
            case .halalRestaurants:
                return 86
            case .allRestaurant, .theWeekOffers,.bestRestaurants:
               return 300
            }
        }
        
    }

    
}

