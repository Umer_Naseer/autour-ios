//
//  RestaurantLocationsViewController.swift
//  autour
//
//  Created by huda elhady on 15/07/2021.
//

import UIKit

struct TitleCountType {
    let title: String
    let count: Int
}

class LocationsListedViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var list: [TitleCountType] = [TitleCountType(title: "Geneve", count: 47), TitleCountType(title: "Lausanne", count: 40), TitleCountType(title: "Zurich", count: 0), TitleCountType(title: "Fribourg", count: 35), TitleCountType(title: "Montreux", count: 21)]
    private var listTitle: String = ""
    private var listType: LocationsListType = .restaurants
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
    }
    
    static func create(listTitle: String, listType: LocationsListType)-> LocationsListedViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.LocationsListedVC.id, storyBoard: StoryBoardsIDs.restaurants.id) as? LocationsListedViewController {
            vc.listTitle = listTitle
            vc.listType = listType
            return vc
        }
        return LocationsListedViewController()
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension LocationsListedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantLocationTableCell.identifier, for: indexPath) as! RestaurantLocationTableCell
        cell.configure(location: list[indexPath.row])
        return cell
    }
    
    
}

extension LocationsListedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(listType.viewController, animated: true)
    }
}
