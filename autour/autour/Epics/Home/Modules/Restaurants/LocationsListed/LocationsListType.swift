//
//  LocationsListType.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit

extension LocationsListedViewController {
    enum LocationsListType {
        case restaurants
        case mosques
        case activities
        case hotels
        
        var viewController: UIViewController {
            switch self {
            case .restaurants:
                return RestaurantsListViewController.create(listTitle: "Pakistani Food" )
            case .mosques:
                return MosquesListViewController.create(listTitle: "Mosques", list: nil )
            case .hotels:
                return HotelsListViewController.create(listTitle: "Lodging" )
            case .activities:
                return ActivityListViewController.create(listTitle: "List of activities" )            
            }
        }
        
    }
}
