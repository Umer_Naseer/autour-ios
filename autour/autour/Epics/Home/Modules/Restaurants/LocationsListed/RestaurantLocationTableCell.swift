//
//  RestaurantLocationTableCell.swift
//  autour
//
//  Created by huda elhady on 15/07/2021.
//

import UIKit

class RestaurantLocationTableCell: UITableViewCell {
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    static let identifier = "RestaurantLocationTableCell"
    
    func configure(location: TitleCountType) {
        countLabel.text = location.count > 0 ? "\(location.count) Listed": "Soon"
        nameLabel.text = location.title
        arrowImage.image = location.count > 0 ? #imageLiteral(resourceName: "green_next"): #imageLiteral(resourceName: "grey_next")
        countLabel.textColor = location.count > 0 ? #colorLiteral(red: 0.1019607843, green: 0.9019607843, blue: 0.6549019608, alpha: 1):UIColor(named: "grey_white")
    }
    
}
