//
//  RestaurantTableCell.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit
import Cosmos

class RestaurantTableCell: UITableViewCell {
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var timeDistanceLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    static let identifier = "RestaurantTableCell"
    
    func configure(result: ReviewType) {
        resultImageView.image = result.image
        timeDistanceLabel.text = "\(result.time) • \(result.distance)KM"
        locationLabel.text = result.location
        nameLabel.text = result.name
        ratingView.rating = result.rating
        ratingView.text = "(\(result.rating))"
    }
    
}

