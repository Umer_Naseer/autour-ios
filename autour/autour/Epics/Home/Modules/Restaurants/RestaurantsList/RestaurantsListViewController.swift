//
//  RestaurantsListViewController.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class RestaurantsListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var list: [ReviewType] = [
        ReviewType(image: #imageLiteral(resourceName: "pizza"), name: "Pizza Lover Company", location: "6085 Hasliberg Goldern, Switzerland", time: "", distance: 1.0, rating: 4.5), ReviewType(image:  #imageLiteral(resourceName: "italian"), name: "Chicken Fried Restaurant", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.3), ReviewType(image: #imageLiteral(resourceName: "italian"), name: "Pizza Lover Company", location: "Route de Tsallion, 1986 Evolène, Switzerland", time: "", distance: 1.5, rating: 4.3)]
    private var listTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
        tableView.register(UINib(nibName: ResultTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ResultTableViewCell.identifier)
    }
    
    static func create(listTitle: String)-> RestaurantsListViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.RestaurantsListVC.id, storyBoard: StoryBoardsIDs.restaurants.id) as? RestaurantsListViewController {
            vc.listTitle = listTitle
            return vc
        }
        return RestaurantsListViewController()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let vc = CommonMethods.createVC(ViewControllersIDs.RestaurantsFilterVC.id, storyBoard: StoryBoardsIDs.restaurants.id)
        self.present(vc, animated: true, completion: nil)
    }
}

extension RestaurantsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewCell.identifier, for: indexPath) as! ResultTableViewCell
        cell.configure(result: list[indexPath.row], bgColor: UIColor(named: "login_main_view")!)
        return cell
    }
    
}

extension RestaurantsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.RestaurantDetailsVC.id, storyBoard: StoryBoardsIDs.restaurants.id), animated: true)
    }
}







