//
//  DiscountViewController.swift
//  autour
//
//  Created by huda elhady on 16/07/2021.
//

import UIKit

class DiscountViewController: UIViewController {
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func homeButtonAction(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}
