//
//  DestinationType.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit

extension FoodTypeViewController {
    enum DestinationType {
        case restaurants
        case mosques
        case activities
        case donation
        case services
        
        var viewController: UIViewController {
            switch self {
            case .restaurants:
                return RestaurantsListViewController.create(listTitle: "Pakistani Food")
            case .mosques:
                return MosquesListViewController.create(listTitle: "Mosques", list: nil )
            case .donation:
                return RestaurantsListViewController.create(listTitle: "Pakistani Food" )
            case .activities:
                return ActivityListViewController.create(listTitle: "List of activities" )
            case .services:
                return ServicesListViewController.create(listTitle: "List of Services")
            }
        }
        
    }
}
