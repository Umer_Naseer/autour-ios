//
//  TypeTableCell.swift
//  autour
//
//  Created by huda elhady on 15/07/2021.
//

import UIKit

class TypeTableCell: UITableViewCell {
    
    @IBOutlet weak var foodImageView: UIImageView!
//    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identefier = "TypeTableCell"
    
       
    
    func configure(type: ImageTitleType) {
//        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        foodImageView.image = type.image
        nameLabel.text = type.title
       
    }
}
