//
//  FoodTypeViewController.swift
//  autour
//
//  Created by huda elhady on 15/07/2021.
//

import UIKit

class FoodTypeViewController: UIViewController {
    
    @IBOutlet weak var categoryCollection:UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var type: DestinationType = .restaurants
    private var listTitle: String = ""
    
    private var list: [Category] = [Category(name: "Italian Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Pakistani Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Thai Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Fast Food", image: #imageLiteral(resourceName: "Italian_food")), Category(name: "Indian Food", image: #imageLiteral(resourceName: "Italian_food"))]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
        let layout = categoryCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 12
        layout.itemSize = CGSize(width:  (self.view.frame.width - 60) / 4 , height: 85)
        categoryCollection.collectionViewLayout = layout
        categoryCollection.reloadData()
    }
    
    static func create(list: [Category], listTitle: String, type: DestinationType)-> FoodTypeViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.FoodTypeVC.id, storyBoard: StoryBoardsIDs.restaurants.id) as? FoodTypeViewController {
            vc.list = list
            vc.type = type
            vc.listTitle = listTitle
            return vc
        }
        return FoodTypeViewController()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension FoodTypeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.identefier, for: indexPath) as! CategoryCell
        cell.configure(category: list[indexPath.row])
        return cell
    }
    
}

extension FoodTypeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.pushViewController(type.viewController, animated: true)
    }
}

