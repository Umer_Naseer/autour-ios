//
//  DuaViewController.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

struct DuaType {
    let name: String
    let image: UIImage
    let chapters: Int
}

class DuaViewController: UIViewController {
    
    @IBOutlet weak var categoryCollection:UICollectionView!
    
    var categories: [DuaType] = [
        DuaType(name: "Daily", image: #imageLiteral(resourceName: "dua_daily"), chapters: 132),
        DuaType(name: "Zikr & Sleep", image: #imageLiteral(resourceName: "dua_zakir"), chapters: 6),
        DuaType(name: "Society", image: #imageLiteral(resourceName: "dua_society"), chapters: 3),
        DuaType(name: "Hajj & Fast", image: #imageLiteral(resourceName: "dua_hajj"), chapters: 100),
        DuaType(name: "Prayer", image: #imageLiteral(resourceName: "dua_prayer"), chapters: 4),
        DuaType(name: "Quranic", image: #imageLiteral(resourceName: "dua_quranic"), chapters: 2),
        DuaType(name: "Feelings", image: #imageLiteral(resourceName: "dua_feelings"), chapters: 200),
        DuaType(name: "Iman", image: #imageLiteral(resourceName: "dua_iman"), chapters: 8),
        DuaType(name: "Illness", image: #imageLiteral(resourceName: "dua_Illness"), chapters: 7)]{
        didSet
        {
            categoryCollection.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = categoryCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 20
        layout.itemSize = CGSize(width:  (self.view.frame.width - 65) / 3 , height: 100)
//        layout.minimumLineSpacing = 10
        categoryCollection.collectionViewLayout = layout
        categoryCollection.reloadData()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension DuaViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DuaCollectionCell.identifier, for: indexPath) as! DuaCollectionCell
        cell.configure(result: categories[indexPath.row])
        return cell
    }
    
}

extension DuaViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.DuaTypeVC.id, storyBoard: StoryBoardsIDs.dua.id), animated: true)
    }
}
