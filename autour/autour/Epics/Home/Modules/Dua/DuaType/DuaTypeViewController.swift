//
//  DuaTypeViewController.swift
//  autour
//
//  Created by huda elhady on 24/07/2021.
//

import UIKit

class DuaTypeViewController: UIViewController {
//    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var resultList: [UIImage] = [#imageLiteral(resourceName: "salah_1"), #imageLiteral(resourceName: "salah_1")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        title = "Search Results"
//        tableView.register(UINib(nibName: ResultTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ResultTableViewCell.identifier)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension DuaTypeViewController: UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return section == 0 ? "Before Salah" : "After Salah"
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DuaTypeTableCell.identifier, for: indexPath) as! DuaTypeTableCell
        cell.configure(image: resultList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor(named: "light_grey_very_black")!
        (view as! UITableViewHeaderFooterView).textLabel?.font = UIFont(name: "Montserrat-Bold", size: 15)
            (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor(named: "black_white")!
    }
    
}

extension DuaTypeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
}
