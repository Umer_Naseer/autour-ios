//
//  DuaTypeTableCell.swift
//  autour
//
//  Created by huda elhady on 24/07/2021.
//

import UIKit

class DuaTypeTableCell: UITableViewCell {
    
    @IBOutlet weak var duaImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    static let identifier = "DuaTypeTableCell"

    func configure(image: UIImage) {
        containerView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 0.2, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        duaImageView.image = image
    }
    
}
