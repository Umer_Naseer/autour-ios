//
//  DuaCollectionCell.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit


class DuaCollectionCell: UICollectionViewCell {
//    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var duaImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var chapterLabel: UILabel!
    static let identifier = "DuaCollectionCell"
    
    func configure(result: DuaType) {
        duaImageView.image = result.image
        titleLabel.text = result.name
    }
    
}
