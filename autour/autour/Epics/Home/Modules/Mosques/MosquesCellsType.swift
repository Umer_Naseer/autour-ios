//
//  MosquesCellsType.swift
//  autour
//
//  Created by huda elhady on 18/07/2021.
//

import UIKit

extension MosquesViewController {
    enum MosquesCellsType {
        case findMosque
        case placesWorship
        case beautifulMosques
        case mosquesNearYou
        case country
        case ideaContact
        
        var title: String {
            switch self {
            case .findMosque, .ideaContact:
                return ""
            case .placesWorship:
                return "Places of worship"
            case .beautifulMosques:
                return "The most beautiful Mosques around the globe"
            case .country:
                return "Get the best experience in those countries"
            case .mosquesNearYou:
                return "Mosques Near You"
            }
        }
        
        var height: CGFloat {
            switch self {
            case .findMosque:
                return 250
            case .placesWorship, .beautifulMosques:
                return 250
            case .ideaContact:
                return 220
            case .country:
                return 330
            case .mosquesNearYou:
                return 86
            }
        }
        
    }
}
