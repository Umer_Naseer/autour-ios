//
//  MosquesListViewController.swift
//  autour
//
//  Created by huda elhady on 01/08/2021.
//

import UIKit

class MosquesListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var list: [Place] = [
        Place( name: "Makki Masjid", place: "78th Street. 88 W 21th St, NY", rating: 4.5, image: #imageLiteral(resourceName: "Makki")),
        Place( name: "Masjid Allah hu Akbar",place: "132th Street. 41 W 10th St, NY", rating: 4.5, image: #imageLiteral(resourceName: "masjid_allah")),
        Place( name: "Bilal Masjid",place: "78th Street. 88 W 21th St, NY", rating: 4.5, image: #imageLiteral(resourceName: "Bilal_masjid"))]
    private var listTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = listTitle
    }
    
    static func create(listTitle: String, list: [Place]?)-> MosquesListViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.MosquesListVC.id, storyBoard: StoryBoardsIDs.Mosques.id) as? MosquesListViewController {
            vc.listTitle = listTitle
            if list != nil {
                vc.list = list!
            }
            return vc
        }
        return MosquesListViewController()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension MosquesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MosqueListTableCell.identifier, for: indexPath) as! MosqueListTableCell
        cell.configure(place: list[indexPath.row])
        return cell
    }
    
}

extension MosquesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MosqueDetailsVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
    }
}

class MosqueListTableCell: UITableViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
//    @IBOutlet weak var placeLabel: UILabel!
//    @IBOutlet weak var distanceLabel: UILabel!
    static let identifier = "MosqueListTableCell"
       
    
    func configure(place: Place, showDistance: Bool = true) {
        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        placeImageView.image = place.image
        nameLabel.text = place.name
//        placeLabel.text = place.place
//        distanceLabel.isHidden = !showDistance
        
    }
}





