//
//  MosquesViewController.swift
//  autour
//
//  Created by huda elhady on 18/07/2021.
//

import UIKit

class MosquesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [MosquesCellsType] = [.findMosque, .placesWorship, .beautifulMosques, .mosquesNearYou, .country,.ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
        tableView.register(UINib(nibName: MosquesNearTableCell.identifier, bundle: nil), forCellReuseIdentifier: MosquesNearTableCell.identifier)
    }

}

extension MosquesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .findMosque:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            let countries = [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
            cell.configure(title: list[indexPath.row].title, list: countries, widthPercentage: 2.3, selectActivityHandler: { [weak self] _ in })

            return cell
        case .placesWorship, .beautifulMosques:

            let cell = tableView.dequeueReusableCell(withIdentifier: MosquesNearTableCell.identifier, for: indexPath) as! MosquesNearTableCell
            let typeList: [Place] = [
                Place( name: "Makki Masjid", place: "78th Street. 88 W 21th St, NY", rating: 4.5, image: #imageLiteral(resourceName: "Makki")),
                Place( name: "Masjid Allah hu Akbar",place: "132th Street. 41 W 10th St, NY", rating: 4.5, image: #imageLiteral(resourceName: "masjid_allah")),
                Place( name: "Bilal Masjid",place: "78th Street. 88 W 21th St, NY", rating: 4.5, image: #imageLiteral(resourceName: "Bilal_masjid"))]
            cell.configure(title: list[indexPath.row].title,
                           list: typeList,widthPercentage: 1.3,
                           selectActivityHandler: {[weak self] _ in self?.openMosqueDetails()},
                           viewAllHandler: {[weak self] in self?.openMosquesList()})
            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        case .mosquesNearYou:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityNearTableCell", for: indexPath)
            return cell
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func openLocationsList(country: ImageTitleType) {
        navigationController?.pushViewController(LocationsListedViewController.create(listTitle: "Listed Mosques", listType: .mosques), animated: true)
    }
    
    func openMosqueDetails() {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MosqueDetailsVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
    }
    
}

extension MosquesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if list[indexPath.row] == .mosquesNearYou {
            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MapVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
        }
    }
}

extension MosquesViewController {
    func openMosquesList() {
        let vc = MosquesListViewController.create(listTitle: "Mosques", list: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
}
