//
//  MapExtension.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import Foundation
import GoogleMaps

extension GMSMapView {
    func styleMap() {
        do {
            if let styleURL = Bundle.main.url(forResource: "mapstyle", withExtension: "json") {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("RAYE7 Unable to find mapstyle.json")
            }
        } catch {
            print("RAYE7 One or more of the map styles failed to load. \(error)")
        }
    }

   static func getCameraPosition() -> GMSCameraPosition {
    return GMSCameraPosition.camera(withLatitude: 40.7560264, longitude: -73.3703311, zoom: MapConstants.cameraZoom)
    }
}
