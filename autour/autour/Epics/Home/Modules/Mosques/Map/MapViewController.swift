//
//  MapViewController.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit
import GoogleMaps

struct MapConstants {
    static let cameraZoom: Float = 10.7
    static let defaltLatitude = 31.01093
    static let defaltLongitude = 31.298609
}

struct PinLocation {
    let lat: Double
    let long: Double
}

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapContainerView: UIView!
    var mapView: GMSMapView!
    var loaded = false
    var circle = GMSCircle(position: CLLocationCoordinate2D(latitude: 40.7560264, longitude: -73.3703311), radius:2000)
    var polyline: GMSPolygon!

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: GMSMapView.getCameraPosition())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        update()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mapView?.clear()
        mapView?.removeFromSuperview()
        mapView = nil
        loaded = false
    }
    
    func loadMap() {
        let frame = CGRect(x: 0, y: 0, width: mapContainerView.frame.width, height: mapContainerView.frame.height)
        mapView = GMSMapView.map(withFrame: frame, camera: GMSMapView.getCameraPosition())
        
        mapView.styleMap()
        mapContainerView.addSubview(mapView)
        
        setConstraints(subview: mapView, parentView: mapContainerView)
        
//        GMSCircle(
        
        
    }
    
    private func setConstraints(subview: UIView, parentView: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        subview.heightAnchor.constraint(equalTo: (parentView.heightAnchor), constant: 0).isActive = true
        subview.widthAnchor.constraint(equalTo: (parentView.widthAnchor), constant: 0).isActive = true
        subview.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 0).isActive = true
        subview.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 0).isActive = true
    }

//    func select(id: Int) {
//        item.updateSelection(id: id)
//        update()
//    }


    func update() {
        if !loaded {
            loaded = true
            loadMap()
        }
        mapView.clear()
        updateMarkers()
    }

    func updateMarkers() {
        
         let bounds = updateMarkersForMatchedDrivers()
        let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.view.frame.height * 0.12) + 100, left: 50, bottom: 20, right: 50))
        mapView.animate(with: update)
        mapView.animate(toZoom: MapConstants.cameraZoom)
        mapView.delegate = self
       
//        let circle = GMSCircle(position: CLLocationCoordinate2D(latitude: 40.7560264, longitude: -73.3703311), radius:70)
        circle.radius = 2000
        circle.position = mapView.projection.coordinate(for:mapView.center)
         circle.fillColor = UIColor(named: "map_center")
         circle.strokeWidth = 3
         circle.strokeColor = #colorLiteral(red: 0.1019607843, green: 0.9019607843, blue: 0.6549019608, alpha: 1)
         circle.map = mapView
        
//        let update1 = GMSCameraUpdate.fit(circle.f)
//        mapView.animate(to: update1)
        
        
        
        
        
    }

    func updateMarkersForMatchedDrivers() -> GMSCoordinateBounds {
        var bounds = GMSCoordinateBounds()
        var zIndex = 0
        
        let pins : [CLLocation] = [CLLocation(latitude: 40.7543077, longitude: -73.3685736),CLLocation(latitude:40.7528403,longitude: -73.66022), CLLocation(latitude:40.7567139,longitude:-73.3674361), CLLocation(latitude:40.7528403,longitude:-73.66022)]

        for wannabe in pins {
            let wannabeMarker = MapMarker()
//                wannabeMarker.setImage(image: wannabe.user.personalImage ?? "")
                bounds = bounds.includingCoordinate(wannabe.coordinate)
                zIndex += 1
                addMarkerWithViewIcon(location: wannabe, icon: wannabeMarker, zIndex: zIndex)

        }

        return bounds
    }


    func addMarkerWithViewIcon(location: CLLocation, icon: UIView, zIndex: Int) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.title = ""
        marker.snippet = ""
        marker.iconView = icon
        marker.zIndex = Int32(zIndex)
        marker.map = mapView
    }

    func addMarkerWithImageIcon(location: CLLocation, icon: UIImage, zIndex: Int) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.title = ""
        marker.snippet = ""
        marker.icon = icon
        marker.zIndex = Int32(zIndex)
        marker.map = mapView
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        circle.map = nil

            let point = mapView.center
            circle.position = mapView.projection.coordinate(for: point)
            circle.radius = 2000
        circle.zIndex = 1
        circle.fillColor = UIColor(named: "map_center")
        circle.strokeWidth = 3
        circle.strokeColor = UIColor(named: "map_center")
        
        circle.map = mapView
        
        if polyline != nil {
            polyline.map = nil
            polyline = nil
        }
        
       
            let path = GMSMutablePath()

            for i in 0...60 {
                let offsetLocation = GMSGeometryOffset(mapView.projection.coordinate(for: point), 300, CLLocationDirection(i*6))
                path.add(offsetLocation)
            }
            polyline = GMSPolygon(path: path)
        polyline.fillColor = #colorLiteral(red: 0.1019607843, green: 0.9019607843, blue: 0.6549019608, alpha: 1)
        polyline.strokeWidth = 2
        polyline.strokeColor = .white
        polyline.map = mapView
        
        
    }
}
