//
//  HomeCellTypes.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

enum HomeCellTypes {
    case heyThere
    case places
    case country
    case hotels
    case ideaContact
    
    var title: String {
        switch self {
        case .places:
            return "Best Destinations"
        case .country:
            return "Get the best experience in those countries"
        case .hotels:
            return "Popular Hotels"
        case .heyThere, .ideaContact:
            return ""
            
        }
    }
    
    var height: CGFloat {
        switch self {
        case .heyThere:
            return 250
        case .places:
            return 330
        case .country:
            return 330
        case .hotels:
            return 330
        case .ideaContact:
            return 220
        }
    }
    
    var widthPercentage: CGFloat {
        switch self {
        case .heyThere, .ideaContact:
            return 1
        case .places:
            return 1.7
        case .country:
            return 2.3
        case .hotels:
            return 1.7
        }
    }
    
    var list: [Place] {
        switch self {
        case .heyThere, .ideaContact:
            return []
        case .places:
            return [Place(name: "Zanzibar", place: "Tanzania", rating: 4.9, image: UIImage(named: "home_destinations")!),Place(name: "Greece", place: "Europe", rating: 4.5, image: #imageLiteral(resourceName: "home_mosque"))]
        case .country:
            return [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
        case .hotels:
            return [Place(name: "Pearl Continental", place: "Lahore", rating: 4.9, image: #imageLiteral(resourceName: "pearl")),Place(name: "Awari Hotel", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "awari"))]
        }
    }
    
    
}

