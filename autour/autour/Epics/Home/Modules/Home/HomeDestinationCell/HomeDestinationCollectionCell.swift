//
//  DestinationCollectionCell.swift
//  autour
//
//  Created by huda elhady on 02/10/2021.
//

import UIKit

class HomeDestinationCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    static let identefier = "HomeDestinationCollectionCell"
       
    
    func configure(place: Place, showRating: Bool = true) {
        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        placeImageView.image = place.image
        nameLabel.text = place.name
        placeLabel.text = place.place
    }
}

