//
//  DestinationCell.swift
//  autour
//
//  Created by huda elhady on 02/10/2021.
//

import UIKit

class HomeDestinationCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [Place]()
    private var widthPercentage: CGFloat = 0
    static let identifier = "HomeDestinationCell"
    private var selectActivityHandler: ((Place)-> Void)?
    private var viewAllHandler: (()-> Void)?

    func configure(title: String, list: [Place], widthPercentage: CGFloat, selectActivityHandler: @escaping (Place)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.list = list
    self.selectActivityHandler = selectActivityHandler
    self.viewAllHandler = viewAllHandler
        self.widthPercentage = widthPercentage
        titleLabel.text = title
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: HomeDestinationCollectionCell.identefier, bundle: nil), forCellWithReuseIdentifier: HomeDestinationCollectionCell.identefier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: collectionView.frame.width / widthPercentage , height: self.contentView.frame.height - 50)
        collectionView.collectionViewLayout = layout
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension HomeDestinationCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeDestinationCollectionCell.identefier, for: indexPath) as! HomeDestinationCollectionCell
        cell.configure(place: list[indexPath.row])
        return cell
    }
    
}

extension HomeDestinationCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectActivityHandler?(list[indexPath.row])
    }
}

