//
//  HomeCategoryViewController.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

struct Category {
    let name:String
    let image: UIImage
}

class HomeCategoryViewController: UIViewController {
    @IBOutlet weak var categoryCollection:UICollectionView!
    
    var didSelectItemHandler: ((Category)->Void)?
    
    
    var categories: [Category] = [Category(name: "Restaurants", image: #imageLiteral(resourceName: "food")),Category(name: "Activities", image: #imageLiteral(resourceName: "hiking")),Category(name: "Holidays", image: #imageLiteral(resourceName: "planner")) ,Category(name: "Hotels", image: #imageLiteral(resourceName: "hotels")), Category(name: "More", image: #imageLiteral(resourceName: "more"))]{
        didSet
        {
            categoryCollection.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = categoryCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width:  (self.view.frame.width - 72) / 5 , height: 79)
        categoryCollection.collectionViewLayout = layout
        categoryCollection.reloadData()
    }
    

}

extension HomeCategoryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.identefier, for: indexPath) as! CategoryCell
        cell.configure(category: categories[indexPath.row])
        return cell
    }
    
}

extension HomeCategoryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        didSelectItemHandler?(categories[indexPath.row])
            if categories[indexPath.row].name == "Restaurants" {
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.RestaurantsVC.id, storyBoard: StoryBoardsIDs.restaurants.id), animated: true)
            } else if categories[indexPath.row].name == "Mosque" {
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MosquesVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
            } else if categories[indexPath.row].name == "Hotels" {
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.HotelsVC.id, storyBoard: StoryBoardsIDs.hotels.id), animated: true)
            } else if categories[indexPath.row].name == "Activities"{
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.ActivityHomeVC.id, storyBoard: StoryBoardsIDs.activity.id), animated: true)
            } else if categories[indexPath.row].name == "Dua"{
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.DuaVC.id, storyBoard: StoryBoardsIDs.dua.id), animated: true)
            }
            else if categories[indexPath.row].name == "Services"{
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.ServicesVC.id, storyBoard: StoryBoardsIDs.services.id), animated: true)
            }else if categories[indexPath.row].name == "Associations"{
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.DonationVC.id, storyBoard: StoryBoardsIDs.donation.id), animated: true)
            } else if categories[indexPath.row].name == "Holidays Planner"{
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.PlanningVC.id, storyBoard: StoryBoardsIDs.planning.id), animated: true)
            } else if categories[indexPath.row].name == "More"{
                navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.moreVC.id, storyBoard: StoryBoardsIDs.more.id), animated: true)
            }
    }
}
