//
//  CategoryCell.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    static let identefier = "CategoryCell"
       
    
    func configure(category: Category) {
        imageContainerView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        categoryImageView.image = category.image
        nameLabel.text = category.name
    }
}
