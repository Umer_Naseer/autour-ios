//
//  SpecificHomeViewController.swift
//  autour
//
//  Created by huda elhady on 02/10/2021.
//

import UIKit

enum SpecificHomeCellTypes {
    case heyThere
    case activity
    case restaurants
    case services
    case country
    case hotels
    case ideaContact
    
    var title: String {
        switch self {
        case .activity:
            return "Popular Activities"
        case .restaurants:
            return "Popular Restaurants"
        case .services:
            return "Popular Services"
        case .country:
            return "Get the best experience in those countries"
        case .hotels:
            return "Popular Hotels in world"
        case .heyThere, .ideaContact:
            return ""
            
        }
    }
    
    var height: CGFloat {
        switch self {
        case .heyThere:
            return 250
        case .activity, .services:
            return 340
        case .restaurants:
            return 340
        case .country:
            return 330
        case .hotels:
            return 330
        case .ideaContact:
            return 220
        }
    }
    
    var widthPercentage: CGFloat {
        switch self {
        case .heyThere, .ideaContact:
            return 1
        case .activity,.services:
            return 1.7
        case .restaurants:
            return 1.7
        case .country:
            return 2.3
        case .hotels:
            return 1.7
        }
    }
    
    var list: [Place] {
        switch self {
        case .heyThere, .ideaContact:
            return []
        case .activity:
            return [Place(name: "Lake Geneva", place: "Nature, Nautical", rating: 4.9, image: #imageLiteral(resourceName: "home_swizerland")),Place(name: "The Jet d’eau", place: "Monument", rating: 4.5, image: #imageLiteral(resourceName: "camping"))]
        case .restaurants:
            return [Place(name: "SafeMeat", place: "Steakhouse, Grill", rating: 4.9, image:#imageLiteral(resourceName: "intro")),Place(name: "The Indus", place: "Pakistani, Indian", rating: 4.5, image: #imageLiteral(resourceName: "italian"))]
        case .services:
            return [Place(name: "Four Seasons", place: "Hotel", rating: 4.9, image:#imageLiteral(resourceName: "awari")),Place(name: "Trendy Villa", place: "Private Villa", rating: 4.5, image: #imageLiteral(resourceName: "hotel_details"))]
        case .country:
            return [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
        case .hotels:
            return [Place(name: "Pearl Continental", place: "Lahore", rating: 4.9, image: #imageLiteral(resourceName: "pearl")),Place(name: "Awari Hotel", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "awari"))]
        }
    }
    
    
}


class SpecificHomeViewController: UIViewController {
//    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var list: [SpecificHomeCellTypes] = [.heyThere, .activity,.restaurants, .services, .hotels,.country, .ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: PopularPlacesCell.identifier, bundle: nil), forCellReuseIdentifier: PopularPlacesCell.identifier)
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        searchView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.filterVC.id, storyBoard: StoryBoardsIDs.filter.id), animated: true)
    }
    

}

extension SpecificHomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .heyThere:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
            
        case .activity, .restaurants:
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularPlacesCell.identifier, for: indexPath) as! PopularPlacesCell
            
            cell.configure(title: list[indexPath.row].title, list: list[indexPath.row].list, widthPercentage: list[indexPath.row].widthPercentage, selectActivityHandler: { [weak self] in self?.openDetails(title: $0.name, index: indexPath.row)}, viewAllHandler: { [weak self] in self?.openList(index: indexPath.row)})
        case .hotels, .services:
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularPlacesCell.identifier, for: indexPath) as! PopularPlacesCell
            
            cell.configure(title: list[indexPath.row].title, list: list[indexPath.row].list, widthPercentage: list[indexPath.row].widthPercentage, showDistance: false, selectActivityHandler: { [weak self] in self?.openDetails(title: $0.name, index: indexPath.row)}, viewAllHandler: { [weak self] in self?.openList(index: indexPath.row)})
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            
            cell.configure(title: list[indexPath.row].title, list: list[indexPath.row].list, widthPercentage: list[indexPath.row].widthPercentage, selectActivityHandler: { [weak self] in self?.openDetails(title: $0.name, index: indexPath.row)})

            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension SpecificHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
}

extension SpecificHomeViewController {
    func openDetails(title: String, index: Int){
        var vc: UIViewController
        switch list[index] {
        case .heyThere, .ideaContact:
            print("")
        case .country:
            vc = CommonMethods.createVC(ViewControllersIDs.RestaurantDetailsVC.id, storyBoard: StoryBoardsIDs.restaurants.id)
            navigationController?.pushViewController(vc, animated: true)
        case .activity, .restaurants, .services:
            vc = DetailsViewController.create(screenTitle: "Activity details", title: title, list: [#imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details")])
            navigationController?.pushViewController(vc, animated: true)
        case .hotels:
            vc = CommonMethods.createVC(ViewControllersIDs.HotelDetailsVC.id, storyBoard: StoryBoardsIDs.hotels.id)
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func openList(index: Int){
        var vc: UIViewController
        switch list[index] {
        case .country:
            vc = RestaurantsListViewController.create(listTitle: "Recommended Restaurants")
            navigationController?.pushViewController(vc, animated: true)
        case .activity:
            vc = ActivityListViewController.create(listTitle: "Popular Activities")
            navigationController?.pushViewController(vc, animated: true)
        case .hotels:
            vc = HotelsListViewController.create(listTitle: "Hotels")
            navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
        
    }
}

