//
//  HomeCountryCell.swift
//  autour
//
//  Created by huda elhady on 02/10/2021.
//

import UIKit

class HomeCountryCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [Place]()
    private var widthPercentage: CGFloat = 0
    static let identifier = "HomeCountryCell"
    private var selectActivityHandler: ((Place)-> Void)?

    func configure(title: String, list: [Place], widthPercentage: CGFloat, selectActivityHandler: @escaping (Place)-> Void) {
        self.list = list
    self.selectActivityHandler = selectActivityHandler
        self.widthPercentage = widthPercentage
        titleLabel.text = title
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: HomeCountryCollectionCell.identefier, bundle: nil), forCellWithReuseIdentifier: HomeCountryCollectionCell.identefier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: collectionView.frame.width / widthPercentage , height: self.contentView.frame.height - 70)
        collectionView.collectionViewLayout = layout
    }

}

extension HomeCountryCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCountryCollectionCell.identefier, for: indexPath) as! HomeCountryCollectionCell
        cell.configure(place: list[indexPath.row])
        return cell
    }
    
}

extension HomeCountryCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectActivityHandler?(list[indexPath.row])
    }
}


