//
//  HomeViewController.swift
//  autour
//
//  Created by huda elhady on 12/07/2021.
//

import UIKit

class HomeViewController: UIViewController {
//    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var list: [HomeCellTypes] = [.heyThere, .places, .hotels, .country, .ideaContact    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: PopularPlacesCell.identifier, bundle: nil), forCellReuseIdentifier: PopularPlacesCell.identifier)
        tableView.register(UINib(nibName: HomeDestinationCell.identifier, bundle: nil), forCellReuseIdentifier: HomeDestinationCell.identifier)
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
        
        tableView.register(UINib(nibName: CountryTableCell.identifier, bundle: nil), forCellReuseIdentifier: CountryTableCell.identifier)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        searchView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.filterVC.id, storyBoard: StoryBoardsIDs.filter.id), animated: true)
    }
    

}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .heyThere:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
        case .places:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeDestinationCell.identifier, for: indexPath) as! HomeDestinationCell
            
            cell.configure(title: list[indexPath.row].title, list: list[indexPath.row].list, widthPercentage: list[indexPath.row].widthPercentage, selectActivityHandler: { [weak self] in self?.openDetails(title: $0.name, index: indexPath.row)}, viewAllHandler: { [weak self] in self?.openList(index: indexPath.row)})
        case .hotels:
            let cell = tableView.dequeueReusableCell(withIdentifier: PopularPlacesCell.identifier, for: indexPath) as! PopularPlacesCell
            
            cell.configure(title: list[indexPath.row].title, list: list[indexPath.row].list, widthPercentage: list[indexPath.row].widthPercentage, showDistance: false, selectActivityHandler: { [weak self] in self?.openDetails(title: $0.name, index: indexPath.row)}, viewAllHandler: { [weak self] in self?.openList(index: indexPath.row)})
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            
            cell.configure(title: list[indexPath.row].title, list: list[indexPath.row].list, widthPercentage: list[indexPath.row].widthPercentage, selectActivityHandler: { [weak self] in self?.openDetails(title: $0.name, index: indexPath.row)})

            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
}

extension HomeViewController {
    func openDetails(title: String, index: Int){
        var vc: UIViewController
        switch list[index] {
        case .heyThere, .ideaContact:
            print("")
        case .country:
            vc = CommonMethods.createVC(ViewControllersIDs.RestaurantDetailsVC.id, storyBoard: StoryBoardsIDs.restaurants.id)
            navigationController?.pushViewController(vc, animated: true)
        case .places:
            vc = DetailsViewController.create(screenTitle: "Activity details", title: title, list: [#imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details"), #imageLiteral(resourceName: "activity_details")])
            navigationController?.pushViewController(vc, animated: true)
        case .hotels:
            vc = CommonMethods.createVC(ViewControllersIDs.HotelDetailsVC.id, storyBoard: StoryBoardsIDs.hotels.id)
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func openList(index: Int){
        var vc: UIViewController
        switch list[index] {
        case .country:
            vc = RestaurantsListViewController.create(listTitle: "Recommended Restaurants")
            navigationController?.pushViewController(vc, animated: true)
        case .places:
            vc = ActivityListViewController.create(listTitle: "Popular Activities")
            navigationController?.pushViewController(vc, animated: true)
        case .hotels:
            vc = HotelsListViewController.create(listTitle: "Hotels")
            navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
        
    }
}
