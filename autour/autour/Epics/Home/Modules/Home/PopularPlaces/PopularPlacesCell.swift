//
//  PopularPlacesCell.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

struct Place {
    let name: String
    let place: String
    let rating: Double
    let image: UIImage
}

class PopularPlacesCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [Place]()
    private var widthPercentage: CGFloat = 0
    static let identifier = "PopularPlacesCell"
    private var selectActivityHandler: ((Place)-> Void)?
    private var viewAllHandler: (()-> Void)?
    private var showDistance = true

    func configure(title: String, list: [Place], widthPercentage: CGFloat, showDistance: Bool = true, selectActivityHandler: @escaping (Place)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.list = list
    self.selectActivityHandler = selectActivityHandler
    self.viewAllHandler = viewAllHandler
        self.widthPercentage = widthPercentage
        titleLabel.text = title
        self.showDistance = showDistance
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: PlaceCollectionCell.identefier, bundle: nil), forCellWithReuseIdentifier: PlaceCollectionCell.identefier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: collectionView.frame.width / widthPercentage , height: self.contentView.frame.height - 50)
        collectionView.collectionViewLayout = layout
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension PopularPlacesCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlaceCollectionCell.identefier, for: indexPath) as! PlaceCollectionCell
        cell.configure(place: list[indexPath.row], showDistance: showDistance)
        return cell
    }
    
}

extension PopularPlacesCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectActivityHandler?(list[indexPath.row])
    }
}
