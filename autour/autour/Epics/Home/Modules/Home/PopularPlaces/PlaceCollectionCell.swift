//
//  PlaceCollectionCell.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit

class PlaceCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    static let identefier = "PlaceCollectionCell"
       
    
    func configure(place: Place, showDistance: Bool = true) {
        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        placeImageView.image = place.image
        nameLabel.text = place.name
        placeLabel.text = place.place

            ratingLabel.text = "\(place.rating)"
            ratingLabel.isHidden = false
        distanceLabel.isHidden = !showDistance
        
    }
}
