//
//  DonationsDetailsViewController.swift
//  autour
//
//  Created by huda elhady on 13/10/2021.
//

import UIKit
import EAIntroView

class DonationsDetailsViewController: UIViewController {
    @IBOutlet weak var introMainView: EAIntroView!
    @IBOutlet weak var screenTitleLabel: UILabel!
    @IBOutlet weak var detailsTitleLabel: UILabel!
    
    var timer: Timer!
    
    private var list = [#imageLiteral(resourceName: "donation_details"), #imageLiteral(resourceName: "donation_details"), #imageLiteral(resourceName: "donation_details")]
    private var detailsTitle = ""
    private var screenTitle = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIntroView()
        screenTitleLabel.text = screenTitle
        detailsTitleLabel.text = detailsTitle
    }
    
    static func create(screenTitle: String, title: String, list: [UIImage])-> DonationsDetailsViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.DonationsDetailsVC.id, storyBoard: StoryBoardsIDs.donation.id) as? DonationsDetailsViewController {
            vc.list = list
            vc.detailsTitle = title
            vc.screenTitle = screenTitle
            return vc
        }
        return DonationsDetailsViewController()
    }
    
    func setupIntroView() {
        
        self.view.layoutIfNeeded()
        list.forEach{
            let page1 = EAIntroPage.init(customViewFromNibNamed: "IntroPage")
            (page1?.customView.viewWithTag(12) as! UIImageView).image = $0
            if self.introMainView.pages != nil {
            self.introMainView.pages.append(page1!)
            } else {
                self.introMainView.pages = [page1!]
            }
        }
        
        self.introMainView.skipButton.isHidden = true
        self.introMainView.limitPageIndex = self.introMainView.pages.count - 1
        self.introMainView.scrollingEnabled = true
        self.introMainView.pageControlY = 27
        self.introMainView.pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.1019607843, green: 0.9019607843, blue: 0.6549019608, alpha: 1)
        timer =  Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { (timer) in
            // Do what you need to do repeatedly
            let scrolledIndex = self.introMainView.currentPageIndex + 1
            if scrolledIndex < self.introMainView.pages.count {
                self.introMainView.scrollToPage(for: scrolledIndex, animated: true)
            }
            else{
                self.introMainView.scrollToPage(for: 0, animated: true)
            }
            
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
