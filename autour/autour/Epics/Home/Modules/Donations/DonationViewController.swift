//
//  DonationViewController.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

//class DonationViewController: UIViewController {
//    @IBOutlet weak var searchView: UIView!
//    @IBOutlet weak var tableView: UITableView!
//
//    var list: [DonationCellsType] = [.ngoType,.charityOrganizations]
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableViewSetup()
//    }
//
//    func tableViewSetup()  {
//        tableView.register(UINib(nibName: FoodTypeTableCell.identifier, bundle: nil), forCellReuseIdentifier: FoodTypeTableCell.identifier)
//        tableView.register(UINib(nibName: MosquesNearTableCell.identifier, bundle: nil), forCellReuseIdentifier: MosquesNearTableCell.identifier)
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        searchView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
//    }
//}
//
//extension DonationViewController: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return list.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        switch list[indexPath.row] {
//        case .ngoType:
//            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTypeTableCell.identifier, for: indexPath) as! FoodTypeTableCell
//            let typeList: [ImageTitleType] = [ImageTitleType(image: #imageLiteral(resourceName: "home_mosque"), title: "Build Masjid"), ImageTitleType(image: #imageLiteral(resourceName: "home_mosque"), title: "Feed the poor"), ImageTitleType(image: #imageLiteral(resourceName: "home_mosque"), title: "Educate Children"), ImageTitleType(image: #imageLiteral(resourceName: "home_mosque"), title: "Eid Gift")]
//            cell.configure(list: typeList, title: list[indexPath.row].title, viewAllHandler: openViewAllDontionsType, selectTypeHandler: {[weak self] in self?.openDonationList(title: $0.title)})
//            return cell
//        case .charityOrganizations:
//            let cell = tableView.dequeueReusableCell(withIdentifier: MosquesNearTableCell.identifier, for: indexPath) as! MosquesNearTableCell
//            let typeList: [MosquesNearYouType] = [
//                MosquesNearYouType(image: #imageLiteral(resourceName: "Makki"), title: "Ommanh Charity", location: "Switzerland, Paris", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ex eros, hendrerit vitae urna eu, congue tempor nulla."), MosquesNearYouType(image: #imageLiteral(resourceName: "masjid_allah"), title: "Edhi Foundations", location: "Paris, France", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ex eros, hendrerit vitae urna eu, congue tempor nulla.")]
//            cell.configure(title: "Charity Organizations", list: typeList, selectMosqueHandler: {[weak self] in self?.openDonationDetails($0)}, viewAllHandler: {[weak self] in })
//            return cell
//
//        }
//    }
//
//    @IBAction func backButtonAction(_ sender: UIButton) {
//        navigationController?.popViewController(animated: true)
//    }
//
//}
//
//extension DonationViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return list[indexPath.row].height
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////
//    }
//}
//
//extension DonationViewController {
//    func openViewAllDontionsType()  {
//        let list: [Category] = [Category(name: "Build Masjid", image: #imageLiteral(resourceName: "donate_2")), Category(name: "Feed the Poor", image: #imageLiteral(resourceName: "donate_3")), Category(name: "Educate Children", image: #imageLiteral(resourceName: "donate_1")), Category(name: "Eid Gift", image: #imageLiteral(resourceName: "donate_1")), Category(name: "Eid Fitrana", image: #imageLiteral(resourceName: "donate_3"))]
//        navigationController?.pushViewController(FoodTypeViewController.create(list: list, listTitle: "Charity", type: .donation), animated: true)
//    }
//}
//
//extension DonationViewController {
//    func openDonationDetails(_ mosque: MosquesNearYouType) {
//        navigationController?.pushViewController(DetailsViewController.create(screenTitle: "NGO Details", title: "Ommah Charity", list: [#imageLiteral(resourceName: "donation_details"), #imageLiteral(resourceName: "donation_details"), #imageLiteral(resourceName: "donation_details")]), animated: true)
//    }
//
//    func openDonationList(title: String) {
//
//    }
//}


class DonationViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var list: [DonationCellsType] = [.findChairty, .charityOrganizations, .country,.ideaContact]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
    }
    
    func tableViewSetup()  {
        tableView.register(UINib(nibName: HomeCountryCell.identifier, bundle: nil), forCellReuseIdentifier: HomeCountryCell.identifier)
        tableView.register(UINib(nibName: ChairtyTableCell.identifier, bundle: nil), forCellReuseIdentifier: ChairtyTableCell.identifier)
    }

}

extension DonationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch list[indexPath.row] {
        case .findChairty:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeyThereCell", for: indexPath)
            return cell
        case .country:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCountryCell.identifier, for: indexPath) as! HomeCountryCell
            let countries = [Place(name: "Switzerland", place: "France", rating: 4.9, image: UIImage(named: "home_swizerland")!),Place(name: "France", place: "Lahore", rating: 4.5, image: #imageLiteral(resourceName: "France")), Place(name: "Belgium", place: "France", rating: 4.9, image: #imageLiteral(resourceName: "montana"))]
            cell.configure(title: list[indexPath.row].title, list: countries, widthPercentage: 2.3, selectActivityHandler: { [weak self] _ in })

            return cell
        case .charityOrganizations:

            let cell = tableView.dequeueReusableCell(withIdentifier: ChairtyTableCell.identifier, for: indexPath) as! ChairtyTableCell
            let typeList: [Place] = [
                Place(name: "Ommanh Charity", place: "Switzerland, Paris", rating: 4.6, image: #imageLiteral(resourceName: "donate_2")),
                Place(name: "Edhi Foundations", place: "Paris, France", rating: 4.6, image: #imageLiteral(resourceName: "donate_1"))]
            cell.configure(title: list[indexPath.row].title,
                           list: typeList,
                           selectMosqueHandler: {[weak self] _ in self?.openDonationDetails()},
                           viewAllHandler: {[weak self] in self?.openMosquesList()})
            return cell
        case .ideaContact:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaContactCell", for: indexPath)
            return cell
        
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func openLocationsList(country: ImageTitleType) {
        let list: [Place] = [Place(name: "Build Masjid", place: "", rating: 4.7,  image: #imageLiteral(resourceName: "donate_2")),
            Place(name: "Feed the Poor",place: "",rating: 4.7, image: #imageLiteral(resourceName: "donate_3")),
            Place(name: "Educate Children",place: "",rating: 4.7, image: #imageLiteral(resourceName: "donate_1")),
            Place(name: "Eid Gift", place: "", rating: 4.7,image: #imageLiteral(resourceName: "donate_1")),
            Place(name: "Eid Fitrana", place: "",rating: 4.7, image: #imageLiteral(resourceName: "donate_3"))]
        navigationController?.pushViewController(MosquesListViewController.create(listTitle: "Associations", list: list), animated: true)
    }
    
    func openDonationDetails() {
        navigationController?.pushViewController(DonationsDetailsViewController.create(screenTitle: "NGO Details", title: "Ommah Charity", list: [#imageLiteral(resourceName: "donation_details"), #imageLiteral(resourceName: "donation_details"), #imageLiteral(resourceName: "donation_details")]), animated: true)
    }
    
}

extension DonationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return list[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if list[indexPath.row] == .mosquesNearYou {
//            navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.MapVC.id, storyBoard: StoryBoardsIDs.Mosques.id), animated: true)
//        }
    }
}

extension DonationViewController {
    func openMosquesList() {
        let list: [Place] = [
            Place(name: "Build Masjid", place: "", rating: 4.7,  image: #imageLiteral(resourceName: "donate_2")),
            Place(name: "Feed the Poor",place: "",rating: 4.7, image: #imageLiteral(resourceName: "donate_3")),
            Place(name: "Educate Children",place: "",rating: 4.7, image: #imageLiteral(resourceName: "donate_1")),
            Place(name: "Eid Gift", place: "", rating: 4.7,image: #imageLiteral(resourceName: "donate_1")),
            Place(name: "Eid Fitrana", place: "",rating: 4.7, image: #imageLiteral(resourceName: "donate_3"))]
        navigationController?.pushViewController(MosquesListViewController.create(listTitle: "Associations", list: list), animated: true)
    }
}

