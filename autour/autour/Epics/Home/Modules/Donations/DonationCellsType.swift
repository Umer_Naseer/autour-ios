//
//  DonationCellsType.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

extension DonationViewController {
    enum DonationCellsType {
        case findChairty
        case charityOrganizations
        case country
        case ideaContact
        
        var title: String {
            switch self {
            case .findChairty, .ideaContact:
            return ""
            case .country:
                return "Get the best experience in those countries"
            case .charityOrganizations:
                return "Associations and NGOs"
            }
        }
        
        var height: CGFloat {
            switch self {
            case .findChairty:
            return 250
            case .country:
                return 330
            case .charityOrganizations:
                return 470
            case .ideaContact:
                return 220
            }
        }
        
    }
}

