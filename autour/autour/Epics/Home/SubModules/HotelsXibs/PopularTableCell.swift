//
//  PopularTableCell.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

class PopularTableCell: UITableViewCell {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [ReviewType]()
    private var selectHotelHandler: ((ReviewType)-> Void)?
    private var viewAllHandler: (()-> Void)?
    private var bgColor: UIColor = UIColor(named: "login_main_view")!
    static let identifier = "PopularTableCell"

    func configure(bgColor: UIColor, list: [ReviewType],title: String = "Popular Hotels", selectHotelHandler: @escaping (ReviewType)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.selectHotelHandler = selectHotelHandler
        self.viewAllHandler = viewAllHandler
        titleLabel.text = title
        self.list = list
        self.bgColor = bgColor
        setupTableView()
        tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: PopularHotelCell.identifier, bundle: nil), forCellReuseIdentifier: PopularHotelCell.identifier)
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension PopularTableCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PopularHotelCell.identifier, for: indexPath) as! PopularHotelCell
        cell.configure(result: list[indexPath.row])
        return cell
    }
    
}

extension PopularTableCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectHotelHandler?(list[indexPath.row])
    }
}
