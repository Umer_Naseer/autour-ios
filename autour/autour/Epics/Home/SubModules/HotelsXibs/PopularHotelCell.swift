//
//  PopularHotelCell.swift
//  autour
//
//  Created by huda elhady on 14/10/2021.
//

import UIKit

class PopularHotelCell: UITableViewCell {
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identifier = "PopularHotelCell"
    
    func configure(result: ReviewType) {
        resultImageView.image = result.image
        nameLabel.text = result.name
    }
    
}
