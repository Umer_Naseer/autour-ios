//
//  ResultTableViewCell.swift
//  autour
//
//  Created by huda elhady on 13/07/2021.
//

import UIKit
import Cosmos

class ResultTableViewCell: UITableViewCell {
    @IBOutlet weak var resultImageView: UIImageView!
//    @IBOutlet weak var timeDistanceLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var mainView: UIView!
    static let identifier = "ResultTableViewCell"
    
    func configure(result: ReviewType, bgColor: UIColor) {
        resultImageView.image = result.image
//        timeDistanceLabel.text = "\(result.time) • \(result.distance)KM"
        locationLabel.text = result.location
        nameLabel.text = result.name
        ratingView.rating = result.rating
        ratingView.text = "(\(result.rating))"
//        mainView.backgroundColor = bgColor
    }
    
}
