//
//  DestinationSelectedCell.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class DestinationSelectedCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    private var list = [String]()
    private var closeButtonHandler: ((String)-> Void)?
    static let identifier = "DestinationSelectedCell"

    func configure(list: [String], closeButtonHandler: @escaping (String)-> Void) {
        self.list = list
        self.closeButtonHandler = closeButtonHandler
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: 90 , height: self.contentView.frame.height - 8)
        collectionView.collectionViewLayout = layout
    }
}

extension DestinationSelectedCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DestinationCollectionCell.identifier, for: indexPath) as! DestinationCollectionCell
        cell.configure(location: list[indexPath.row])
        return cell
    }
    
}

extension DestinationSelectedCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        selectRestaurantHandler?(list[indexPath.row])
    }
}


