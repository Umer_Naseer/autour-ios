//
//  PlanDestinationCell.swift
//  autour
//
//  Created by huda elhady on 30/07/2021.
//

import UIKit

class PlanDestinationCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImageView: UIImageView!

    func configure(title: String, image: UIImage) {
        titleImageView.image = image
        titleLabel.text = title
    }
    
}
