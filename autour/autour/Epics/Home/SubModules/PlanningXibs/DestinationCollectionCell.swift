//
//  DestinationCollectionCell.swift
//  autour
//
//  Created by huda elhady on 31/07/2021.
//

import UIKit

class DestinationCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var locationLabel: UILabel!
    static let identifier = "DestinationCollectionCell"
    
    func configure(location: String) {
        locationLabel.text = location
    }
    
}
