//
//  PlanDepatureCell.swift
//  autour
//
//  Created by huda elhady on 30/07/2021.
//

import UIKit

class PlanDepatureCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImageView: UIImageView!
//    private var selectHotelHandler: ((ReviewType)-> Void)?
//    static let identifier = "PopularTableCell"

    func configure(title: String, image: UIImage) {
        titleImageView.image = image
        titleLabel.text = title
    }
    
}
