//
//  PlanTrustCell.swift
//  autour
//
//  Created by huda elhady on 14/10/2021.
//

import UIKit

class PlanTrustCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [ReviewType]()
    private var selectRestaurantHandler: ((ReviewType)-> Void)?
    private var viewAllHandler: (()-> Void)?
    static let identifier = "PlanTrustCell"

    func configure(list: [ReviewType], selectRestaurantHandler: @escaping (ReviewType)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.list = list
        self.selectRestaurantHandler = selectRestaurantHandler
        self.viewAllHandler = viewAllHandler
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: PlanCollectionCell.identifier, bundle: nil), forCellWithReuseIdentifier: PlanCollectionCell.identifier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: ((self.contentView.frame.width - 54) / 1.5) , height: self.contentView.frame.height - 50)
        collectionView.collectionViewLayout = layout
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension PlanTrustCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlanCollectionCell.identifier, for: indexPath) as! PlanCollectionCell
        cell.configure(result: list[indexPath.row])
        return cell
    }
    
}

extension PlanTrustCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectRestaurantHandler?(list[indexPath.row])
    }
}



