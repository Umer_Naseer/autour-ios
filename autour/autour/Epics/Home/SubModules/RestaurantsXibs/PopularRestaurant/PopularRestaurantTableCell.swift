//
//  PopularRestaurantTableCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

class PopularRestaurantTableCell: UITableViewCell {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [ReviewType]()
    static let identifier = "PopularRestaurantTableCell"
    private var selectRestaurantHandler: ((ReviewType)-> Void)?
    private var viewAllHandler: (()-> Void)?

    func configure(list: [ReviewType], selectRestaurantHandler: @escaping (ReviewType)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.list = list
        self.selectRestaurantHandler = selectRestaurantHandler
        self.viewAllHandler = viewAllHandler
        setupTableView()
        tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: ReviewTableCell.identifier, bundle: nil), forCellReuseIdentifier: ReviewTableCell.identifier)
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension PopularRestaurantTableCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableCell.identifier, for: indexPath) as! ReviewTableCell
        cell.configure(result: list[indexPath.row])
        return cell
    }
    
    
}

extension PopularRestaurantTableCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectRestaurantHandler?(list[indexPath.row])
    }
}
