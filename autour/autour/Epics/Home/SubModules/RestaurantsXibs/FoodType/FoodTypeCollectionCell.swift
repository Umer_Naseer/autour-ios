//
//  FoodTypeCollectionCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

class FoodTypeCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var foodImageView: UIImageView!
//    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identefier = "FoodTypeCollectionCell"
       
    
    func configure(food: ImageTitleType) {
//        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        foodImageView.image = food.image
        nameLabel.text = food.title
       
    }
}

