//
//  FoodTypeTableCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//
import UIKit

struct ImageTitleType {
    let image: UIImage
    let title: String
}

class FoodTypeTableCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [ImageTitleType]()
//    private var widthPercentage: CGFloat = 0
    static let identifier = "FoodTypeTableCell"
    private var viewAllHandler: (()->Void)?
    private var selectTypeHandler: ((ImageTitleType)->Void)?
    

    func configure(list: [ImageTitleType], title: String = "Type of Food", viewAllHandler: @escaping ()->Void, selectTypeHandler: @escaping (ImageTitleType)->Void) {
        self.viewAllHandler = viewAllHandler
        self.list = list
        self.selectTypeHandler = selectTypeHandler
        titleLabel.text = title
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: FoodTypeCollectionCell.identefier, bundle: nil), forCellWithReuseIdentifier: FoodTypeCollectionCell.identefier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 4
        layout.itemSize = CGSize(width: contentView.frame.width / 4.2 , height: self.contentView.frame.height - 40)
        collectionView.collectionViewLayout = layout
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension FoodTypeTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodTypeCollectionCell.identefier, for: indexPath) as! FoodTypeCollectionCell
        cell.configure(food: list[indexPath.row])
        return cell
    }
    
}

extension FoodTypeTableCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectTypeHandler?(list[indexPath.row])
    }
}

