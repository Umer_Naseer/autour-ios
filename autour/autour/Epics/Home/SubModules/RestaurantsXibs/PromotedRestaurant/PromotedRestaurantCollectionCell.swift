//
//  PromotedRestaurantCollectionCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit
import Cosmos

class PromotedRestaurantCollectionCell: UICollectionViewCell {
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    static let identifier = "PromotedRestaurantCollectionCell"
    
    func configure(result: ReviewType) {
        restaurantImageView.image = result.image
        locationLabel.text = result.location
        nameLabel.text = result.name
        ratingView.rating = result.rating
        ratingView.text = "(\(result.rating))"
    }
    
}
