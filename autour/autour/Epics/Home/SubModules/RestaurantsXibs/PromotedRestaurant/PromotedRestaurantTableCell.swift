//
//  PromotedRestaurantTableCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

class PromotedRestaurantTableCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [ReviewType]()
    private var selectRestaurantHandler: ((ReviewType)-> Void)?
    private var viewAllHandler: (()-> Void)?
    static let identifier = "PromotedRestaurantTableCell"

    func configure(list: [ReviewType], selectRestaurantHandler: @escaping (ReviewType)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.list = list
        self.selectRestaurantHandler = selectRestaurantHandler
        self.viewAllHandler = viewAllHandler
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: PromotedRestaurantCollectionCell.identifier, bundle: nil), forCellWithReuseIdentifier: PromotedRestaurantCollectionCell.identifier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: ((self.contentView.frame.width - 54) / 2) , height: self.contentView.frame.height - 50)
        collectionView.collectionViewLayout = layout
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }
}

extension PromotedRestaurantTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PromotedRestaurantCollectionCell.identifier, for: indexPath) as! PromotedRestaurantCollectionCell
        cell.configure(result: list[indexPath.row])
        return cell
    }
    
}

extension PromotedRestaurantTableCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectRestaurantHandler?(list[indexPath.row])
    }
}


