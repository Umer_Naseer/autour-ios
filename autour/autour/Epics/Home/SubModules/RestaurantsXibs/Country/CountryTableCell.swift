//
//  CountryTableCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//
import UIKit

class CountryTableCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [ImageTitleType]()
    static let identifier = "CountryTableCell"
    private var selectCountryHandler: ((ImageTitleType)-> Void)?

    func configure(list: [ImageTitleType], selectCountryHandler: @escaping (ImageTitleType)-> Void, backgroundColor: UIColor = .systemBackground) {
        contentView.backgroundColor = backgroundColor
        self.list = list
        self.selectCountryHandler = selectCountryHandler
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: CountryCollectionCell.identefier, bundle: nil), forCellWithReuseIdentifier: CountryCollectionCell.identefier)

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 3
        layout.itemSize = CGSize(width: (self.contentView.frame.width - 50) / 3 , height: self.contentView.frame.height - 50)
        collectionView.collectionViewLayout = layout
    }
}

extension CountryTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CountryCollectionCell.identefier, for: indexPath) as! CountryCollectionCell
        cell.configure(country: list[indexPath.row])
        return cell
    }
    
}

extension CountryTableCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectCountryHandler?(list[indexPath.row])
    }
}


