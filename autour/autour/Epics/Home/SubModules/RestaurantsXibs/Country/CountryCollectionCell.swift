//
//  CountryCollectionCell.swift
//  autour
//
//  Created by huda elhady on 14/07/2021.
//

import UIKit

class CountryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identefier = "CountryCollectionCell"
       
    
    func configure(country: ImageTitleType) {
        countryImageView.image = country.image
        nameLabel.text = country.title
    }
}
