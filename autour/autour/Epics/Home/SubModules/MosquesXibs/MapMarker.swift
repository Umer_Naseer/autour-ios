//
//  MapMarker.swift
//  autour
//
//  Created by huda elhady on 23/07/2021.
//

import UIKit

class MapMarker: UIView {

    @IBOutlet weak var userImage: UIImageView!

    @IBOutlet weak var markerBackgroundView: RoundView!
    

    init(size: CGSize = CGSize(width: 43, height: 52), image: UIImage = #imageLiteral(resourceName: "majid_pin")) {
        let frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        super.init(frame: frame)
        commonInit()
        userImage.image = image 
        setMarkerSize(height: size.height, width: size.width)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
       let view = Bundle.main.loadNibNamed("MapMarker", owner: self, options: nil)?.first as? UIView
        addSubview(view ?? UIView())
        view?.frame = bounds
        view?.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
    }

//    func setImage(image: String) {
//        let url = URL(string: image)
//        userImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "bg_user_default_pic"))
//    }

//    func setMarkerBackgroundColor(_ color: UIColor) {
//        markerBackgroundView.backgroundColor = color
//    }

    func setMarkerSize(height: CGFloat, width: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
}
