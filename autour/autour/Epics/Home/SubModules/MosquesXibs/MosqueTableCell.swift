//
//  MosqueTableCell.swift
//  autour
//
//  Created by huda elhady on 18/07/2021.
//

import UIKit

class MosqueTableCell: UICollectionViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
//    @IBOutlet weak var placeLabel: UILabel!
//    @IBOutlet weak var distanceLabel: UILabel!
    static let identefier = "MosqueTableCell"
       
    
    func configure(place: Place, showDistance: Bool = true) {
        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        placeImageView.image = place.image
        nameLabel.text = place.name
//        placeLabel.text = place.place
//        distanceLabel.isHidden = !showDistance
        
    }
}


