//
//  ChairtyTableCell.swift
//  autour
//
//  Created by huda elhady on 13/10/2021.
//

import UIKit
class ChairtyTableCell: UITableViewCell {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    private var list = [Place]()
    static let identifier = "ChairtyTableCell"
    private var selectMosqueHandler: ((Place)-> Void)?
    private var viewAllHandler: (()-> Void)?

    func configure(title: String, list: [Place], selectMosqueHandler: @escaping (Place)-> Void, viewAllHandler: @escaping ()-> Void) {
        self.list = list
        self.titleLabel.text = title
        self.selectMosqueHandler = selectMosqueHandler
        self.viewAllHandler = viewAllHandler
        setupTableView()
        tableView.reloadData()
    }
    func setupTableView() {
        tableView.register(UINib(nibName: ChirtyCell.identifier, bundle: nil), forCellReuseIdentifier: ChirtyCell.identifier)
    }
    
    @IBAction func viewAllButtonAction(_ sender: UIButton) {
        viewAllHandler?()
    }

}

extension ChairtyTableCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChirtyCell.identifier, for: indexPath) as! ChirtyCell
        cell.configure(place: list[indexPath.row])
        return cell

    }
}

extension ChairtyTableCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectMosqueHandler?(list[indexPath.row])

    }
}
