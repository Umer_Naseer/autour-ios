//
//  ChirtyCell.swift
//  autour
//
//  Created by huda elhady on 13/10/2021.
//

import UIKit

class ChirtyCell: UITableViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
//    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    static let identifier = "ChirtyCell"
    
    func configure(place: Place, showDistance: Bool = true) {
//        bgView.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, layerRadius: 12)
        placeImageView.image = place.image
        nameLabel.text = place.name
    }
}
