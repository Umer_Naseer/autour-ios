//
//  BestLocationsViewController.swift
//  autour
//
//  Created by huda elhady on 02/10/2021.
//

import UIKit

class BestLocationsViewController: UIViewController {
    let list = ["Geneva","Lausanne","Lausanne","Lose King"]
    var selectedList = [String]()
    private var didSelectDestinationsHandler: (([String])-> Void)!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarContainter: UIView!
    @IBOutlet weak var restOfWorldButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchBarContainter.dropShadow(color: UIColor(named: "home_shodow")!, opacity: 0.7, offSet: CGSize(width: -1, height: 1), radius: 10)
    }
    
    func initUI() {
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.systemBackground
        searchBar.placeholder = "Search"
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.systemBackground.cgColor
        searchBar.layer.cornerRadius = 10
        let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        clearButton.tintColor = .white
        
        let text = "Rest of the World".formatAsAttributedString(fontSize: 13,
                                                                    fontColor: UIColor(named: "start_label")!,mainFont: "Montserrat-Regular",
                                                                    boldStringArray: [(string: "Rest of the World", fontSize: 13, fontColor: UIColor(named: "grey_white")!, font: "Montserrat-Bold", isUnderline: true)])
        restOfWorldButton.setAttributedTitle(text, for: .normal)
    }
    
//    static func create(didSelectDestinationsHandler: @escaping ([String])-> Void)-> DestinationViewController {
//        if let vc = CommonMethods.createVC(ViewControllersIDs.DestinationVC.id, storyBoard: StoryBoardsIDs.planning.id) as? DestinationViewController {
//            vc.didSelectDestinationsHandler = didSelectDestinationsHandler
//            return vc
//        }
//        return DestinationViewController()
//    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        didSelectDestinationsHandler?(selectedList)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func restOfWorldButtonAction(_ sender: UIButton) {
        CommonMethods.setArRootVC(CommonMethods.createVC(ViewControllersIDs.RestOfWorldVC.id, storyBoard: StoryBoardsIDs.discover.id))
    }
}

extension BestLocationsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: DestinationTableCell.identifier, for: indexPath) as! DestinationTableCell
        let image = selectedList.contains( list[indexPath.row]) ? #imageLiteral(resourceName: "grey_green_circle"): nil
        cell.configure(title: list[indexPath.row], image: image)
            return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Switzerland"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.1843137255, blue: 0.231372549, alpha: 1)
            (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        
    }
    
}

extension BestLocationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedList.append(list[indexPath.row])
        tableView.reloadData()
    }
}

extension BestLocationsViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        performSearch(searchText: searchBar.text ?? "")
    }
    
    func performSearch(searchText: String) {

    }

}


