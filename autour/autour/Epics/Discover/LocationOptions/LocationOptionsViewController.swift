//
//  LocationOptionsViewController.swift
//  autour
//
//  Created by huda elhady on 01/10/2021.
//

import UIKit

class LocationOptionsViewController: UIViewController {
    @IBOutlet weak var restOfWorldButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        let text = "Rest of the World".formatAsAttributedString(fontSize: 13,
                                                                    fontColor: UIColor(named: "start_label")!,mainFont: "Montserrat-Regular",
                                                                    boldStringArray: [(string: "Rest of the World", fontSize: 13, fontColor: UIColor(named: "grey_white")!, font: "Montserrat-Bold", isUnderline: true)])
        restOfWorldButton.setAttributedTitle(text, for: .normal)
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func restOfWorldButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func locationsExperienceButtonAction(_ sender: UIButton) {
        
        CommonMethods.setArRootVC(CommonMethods.createVC(ViewControllersIDs.BestLocationsVC.id, storyBoard: StoryBoardsIDs.discover.id))
    }
    
    
}

