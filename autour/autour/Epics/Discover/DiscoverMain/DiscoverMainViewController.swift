//
//  DiscoverMainViewController.swift
//  autour
//
//  Created by huda elhady on 01/10/2021.
//

import UIKit

class DiscoverMainViewController: UIViewController {
    @IBOutlet weak var chooseManuallyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        let text = "Choose manually".formatAsAttributedString(fontSize: 13,
                                                                    fontColor: UIColor(named: "start_label")!,mainFont: "Montserrat-Regular",
                                                                    boldStringArray: [(string: "Choose manually", fontSize: 13, fontColor: UIColor(named: "grey_white")!, font: "Montserrat-Bold", isUnderline: true)])
        chooseManuallyButton.setAttributedTitle(text, for: .normal)
        
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chooseManuallyButtonAction(_ sender: UIButton) {
        CommonMethods.setArRootVC(CommonMethods.createVC(ViewControllersIDs.LocationOptionsVC.id, storyBoard: StoryBoardsIDs.discover.id))
    }
    
    
    
}
