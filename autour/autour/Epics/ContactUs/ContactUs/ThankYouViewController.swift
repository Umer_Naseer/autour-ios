//
//  ThankYouViewController.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

class ThankYouViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    private var thankYouMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = thankYouMessage
    }
    
    static func create(message: String)-> ThankYouViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.ThankYouVC.id, storyBoard: StoryBoardsIDs.profile.id) as? ThankYouViewController {
            vc.thankYouMessage = message
            return vc
        }
        return ThankYouViewController()
    }
    
    @IBAction func homeButtonAction(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}
