//
//  ContactUsViewController.swift
//  autour
//
//  Created by huda elhady on 26/07/2021.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contactUsButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(ThankYouViewController.create(message: "We will be in Touch Shortly"), animated: true)
    }
}

