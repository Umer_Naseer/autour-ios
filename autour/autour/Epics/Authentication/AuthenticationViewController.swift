//
//  AuthenticationViewController.swift
//  autour
//
//  Created by huda elhady on 11/07/2021.
//

import UIKit

class AuthenticationViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
    }
    
    func setBackButton()  {
        let backBTN = UIBarButtonItem(image: #imageLiteral(resourceName: "back"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        navigationItem.leftBarButtonItem = backBTN
//        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
}
