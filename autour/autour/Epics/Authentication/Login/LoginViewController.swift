//
//  LoginViewController.swift
//  autour
//
//  Created by huda elhady on 09/07/2021.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var signupTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        
        signupTextView.delegate = self
        let signupText = msgSignup.formatAsAttributedString(fontSize: 16, fontColor: UIColor(named: "start_label")!,mainFont: "Montserrat-Regular", boldStringArray: [(string: "Sign Up", fontSize: 16, fontColor: UIColor(named: "green_white_label")!, font: "Montserrat-Bold" , isUnderline: false)])
        signupText.setAsLink(textToFind: "Sign")
        signupText.setAsLink(textToFind: "Up")
        signupTextView.attributedText = signupText
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }

    @IBAction func loginButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.VerificationCodeVC.id, storyBoard: StoryBoardsIDs.authentication.id), animated: true)
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.forgotPasswordVC.id, storyBoard: StoryBoardsIDs.authentication.id), animated: true)
    }

}

extension LoginViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.signupVC.id, storyBoard: StoryBoardsIDs.authentication.id), animated: true)
        return false
    }

}
