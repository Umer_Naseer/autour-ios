//
//  SignupViewController.swift
//  autour
//
//  Created by huda elhady on 10/07/2021.
//

import UIKit

class SignupViewController: AuthenticationViewController {
    
    @IBOutlet weak var loginTextView: UITextView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailImageView: UIImageView!
    
    var type: SignupType = .email
    
    enum SignupType {
        case email
        case phone
        
        var placeHolder: String {
            switch self {
            case .email:
                return "Enter your e-mail"
            case .phone:
                return "Enter your phone number"
            }
        }
        
        var image: UIImage {
            switch self {
            case .email:
                return UIImage(named: "mail")!
            case .phone:
                return UIImage(named: "phone_grey")!
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    static func create(type: SignupType)-> SignupViewController {
        if let vc = CommonMethods.createVC(ViewControllersIDs.signupVC.id, storyBoard: StoryBoardsIDs.authentication.id) as? SignupViewController {
            vc.type = type
            return vc
        }
        return SignupViewController()
    }
    
    func setupUI() {
        loginTextView.delegate = self
        let signupText = msgLogin.formatAsAttributedString(fontSize: 16, fontColor: UIColor(named: "start_label")!,mainFont: "Montserrat-Regular", boldStringArray: [(string: "Login", fontSize: 16, fontColor: UIColor(named: "green_white_label")!, font: "Montserrat-Bold" , isUnderline: false)])
        signupText.setAsLink(textToFind: "Login")
        loginTextView.attributedText = signupText
        
        emailTextField.placeholder = type.placeHolder
        emailImageView.image = type.image
    }

    @IBAction func signupButtonAction(_ sender: UIButton) {
        CommonMethods.setArRootVC(CommonMethods.createVC(ViewControllersIDs.DiscoverMainVC.id, storyBoard: StoryBoardsIDs.discover.id))
    }

}

extension SignupViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        navigationController?.pushViewController(CommonMethods.createVC(ViewControllersIDs.loginVC.id, storyBoard: StoryBoardsIDs.authentication.id), animated: true)
        return false
    }
}

