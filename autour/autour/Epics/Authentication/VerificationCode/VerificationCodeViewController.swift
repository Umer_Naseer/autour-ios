//
//  VerificationCodeViewController.swift
//  autour
//
//  Created by huda elhady on 11/07/2021.
//

import UIKit
import OTPInputView

class VerificationCodeViewController: AuthenticationViewController {
    
    
    @IBOutlet weak var verificationCodeView: OTPInputView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        verificationCodeView.subviews.forEach{
            ($0 as? UIStackView)?.spacing = 20
            
            $0.subviews.forEach{ item in
                print(item)
                item.backgroundColor = .clear
                item.borderColor = UIColor(named: "start_label")
                item.borderWidth = 1
                print(item.bounds)
                print(item.bounds.midX)
                    var bottomLine = CALayer()
                bottomLine.frame = CGRect(x: 9, y: verificationCodeView.bounds.height - 8, width: (verificationCodeView.bounds.width - 167) / 4, height: 1)
                    bottomLine.backgroundColor = UIColor(named: "start_label")!.cgColor
                print(item.bounds.maxX)
                    item.layer.addSublayer(bottomLine)
                
               
            }
        }
    }
    

    @IBAction func continueButtonAction(_ sender: UIButton) {
        
        
        CommonMethods.setArRootVC(CommonMethods.createVC(ViewControllersIDs.DiscoverMainVC.id, storyBoard: StoryBoardsIDs.discover.id))
//        CommonMethods.setArRootVC(CommonMethods.createVC(ViewControllersIDs.HomeTabBarController.id, storyBoard: StoryBoardsIDs.HomeTab.id))
    }

}
