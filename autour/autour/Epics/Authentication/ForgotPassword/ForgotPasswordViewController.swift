//
//  ForgotPasswordViewController.swift
//  autour
//
//  Created by huda elhady on 10/07/2021.
//

import UIKit

class ForgotPasswordViewController: AuthenticationViewController {
    
//    @IBOutlet weak var signupDescriptionTextView: UITextView!
//    @IBOutlet weak var loginTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func sendLinkButtonAction(_ sender: UIButton) {
        let vc =  UIStoryboard(name: StoryBoardsIDs.authentication.id, bundle: nil).instantiateViewController(withIdentifier: ViewControllersIDs.resetPasswordVC.id)
        navigationController?.pushViewController(vc, animated: true)
    }


}

