//
//  SignupOptionsView.swift
//  autour
//
//  Created by huda elhady on 01/10/2021.
//

import UIKit

class SignupOptionsViewController: AuthenticationViewController {
    
    @IBOutlet weak var termsConditionsTextView: UITextView!
//    @IBOutlet weak var loginTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        let loginText = msgTermsConditions.formatAsAttributedString(fontSize: 13,
                                                                    fontColor: UIColor(named: "start_label")!,mainFont: "Montserrat-Regular",
                                                                    boldStringArray: [(string: "Terms of Use", fontSize: 13, fontColor: UIColor(named: "grey_white")!, font: "Montserrat-Regular", isUnderline: true), (string: "Privacy and Cookie Statement", fontSize: 13, fontColor: UIColor(named: "grey_white")!, font: "Montserrat-Regular", isUnderline: true)])
        termsConditionsTextView.attributedText = loginText
        
        termsConditionsTextView.delegate = self
    }

    @IBAction func signupEmailButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(SignupViewController.create(type: .email), animated: true)
    }
    
    @IBAction func signupPhoneButtonAction(_ sender: UIButton) {
        navigationController?.pushViewController(SignupViewController.create(type: .phone), animated: true)
    }
    
    @IBAction func signupGoogleButtonAction(_ sender: UIButton) {

    }

}

extension SignupOptionsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
//        navigationController?.popViewController(animated: true)
        return false
    }
}

